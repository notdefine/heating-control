<?php
declare(strict_types=1);

namespace HeatingControl\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * Assigned needed water temperature for given outside temperatures
 *
 * @Entity
 * @Table(name="desired_values")
 */
class TemperatureMappingEntity
{
    /**
     * Dabase primary Key
     *
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Outside Temp
     *
     * @var string
     * @Column(type="decimal", precision=2, scale=0)
     */
    protected $outside_temp;

    /**
     * ... and desired water temperature for the outside_temp
     *
     * @var string
     * @Column(type="decimal", precision=2, scale=0)
     */
    protected $return_flow_temp;

    /**
     * @return int
     */
    public function getOutsideTemp(): int
    {
        return (int)$this->outside_temp;
    }

    /**
     * @param string $outside_temp
     */
    public function setOutsideTemp(string $outside_temp): void
    {
        $this->outside_temp = $outside_temp;
    }

    /**
     * @return int
     */
    public function getReturnFlowTemp(): int
    {
        return (int)$this->return_flow_temp;
    }

    /**
     * @param string $return_flow_temp
     */
    public function setReturnFlowTemp(string $return_flow_temp): void
    {
        $this->return_flow_temp = $return_flow_temp;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
