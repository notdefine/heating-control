<?php

namespace HeatingControl\Entity;

use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;

/**
 * Configuration of Sensors, their names and UID's
 *
 * @Entity
 * @Table(name="sensor_settings")
 */
class SensorSettingEntity
{
    /**
     * Dabase primary Key => Used for Mapping RRDTool sensor{id}
     *
     * @var integer
     *
     * @Id
     * @Column(name="id", type="integer")
     * @GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * Family-Code + Identification Example 28-000008fc6545
     *
     * @var string
     * @Column(type="string", length=64)
     */
    protected $identification;
    /**
     * @var string
     * @Column(type="string", length=64)
     */
    protected $name;

    public function toJson()
    {
        return json_encode([
            'id' => $this->getId(),
            'identification' => $this->getIdentification(),
            'name' => $this->getName()
        ]);
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getIdentification(): string
    {
        return $this->identification;
    }

    /**
     * @param string $identification
     */
    public function setIdentification($identification)
    {
        $this->identification = $identification;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
}
