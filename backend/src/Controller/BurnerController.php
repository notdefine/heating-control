<?php
/**
 * @author Thomas Eimers <notdefine@gmx.de>
 */

namespace HeatingControl\Controller;

use HeatingControl\Service\HeatingManagementService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response;

class BurnerController
{
    /**
     * @var HeatingManagementService
     */
    protected $heatingManagementService;

    public function __construct(HeatingManagementService $heatingManagementService)
    {
        $this->heatingManagementService = $heatingManagementService;
    }

    public function index(Request $request, Response $response, array $args): Response
    {
        $string = json_encode(
            [
                'burnerStatus' => $this->heatingManagementService->getLastBurnerStatus(),
                'pumpStatus' => $this->heatingManagementService->getLastPumpStatus(),
                'outsideTemp' => $this->heatingManagementService->getCurrentOutsideTemp(),
                'currentWaterTemp' => $this->heatingManagementService->getCurrentReturnFlowTemp(),
                'targetWaterTemp' => $this->heatingManagementService->getCalculatetTargetReturnFlowTemp()
            ]
        );
        $response->getBody()->write(
            (string)$string
        );
        return $response->withStatus(200);
    }
}
