<?php
/**
 * @author Thomas Eimers <notdefine@gmx.de>
 */

namespace HeatingControl\Controller;

use HeatingControl\Entity\SensorSettingEntity;
use HeatingControl\Service\SensorSettingService;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Http\Response;

class SensorSettingController
{
    /** @var  SensorSettingService */
    protected $sensorSettingService;

    public function __construct(SensorSettingService $sensorSettingService)
    {
        $this->sensorSettingService = $sensorSettingService;
    }

    public function id(Request $request, Response $response, array $args): Response
    {
        $sensorIdentification = $args['id'];

        $sensorSetting = $this->sensorSettingService->getSettingByIdentification($sensorIdentification);

        if ($request->getMethod() === 'GET') {
            // Only for read request it is a failure when the sensor does not already exists
            if ($sensorSetting === false) {
                $response->getBody()->write('Sensor : "' . $sensorIdentification . '" does not exists');
                return $response->withStatus(400);
            }
            $response->getBody()->write($sensorSetting->toJson());
            return $response->withStatus(200);
        }

        if ($request->getMethod() === 'POST') {
            // Sensor is not already configured
            if ($sensorSetting === false) {
                $sensorSetting = new SensorSettingEntity();
            }

            $allPostPutVars = (array)$request->getParsedBody();
            foreach ($allPostPutVars as $key => $param) {
                if (in_array($key, ['id', 'identification', 'name'])) {
                    $functionName = 'set' . ucfirst($key);
                    $sensorSetting->$functionName($param);
                }
            }
            $this->sensorSettingService->setSetting($sensorSetting);
            return $response->withJson(true)->withStatus(200);
        }

        return $response->withStatus(400);
    }
}
