<?php

namespace HeatingControl\Controller;

use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\StateCacheService;
use Mosquitto\Message;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;
use function error_log;

class CliQueueWatcherController
{
    /** @var MessageQueueService */
    protected $messageService;

    /** @var StateCacheService */
    protected $stateCacheService;

    public function __construct(MessageQueueService $messageService, StateCacheService $stateCacheService)
    {
        $this->messageService = $messageService;
        $this->stateCacheService = $stateCacheService;
    }

    public function index(Request $request, Response $response, array $args)
    {
        $this->messageService->onMessage(function (Message $message) {
            error_log(serialize($message));
            // O:17:"Mosquitto\Message":5:{s:3:"mid";i:0;s:5:"topic";s:29:"homeassistant/switch/11/state";s:7:"payload";s:3:"OFF";s:3:"qos";i:0;s:6:"retain";b:0;}
            if ($message->topic == 'homeassistant/switch/11/state') {
                error_log('Eco Mode: ' . $message->payload);
                $this->stateCacheService->set('eco', ($message->payload == 'ON'));
            }
        });

        echo "Wait for Events on Message Queue" . PHP_EOL;
        ob_flush();
        $this->messageService->loopForever();
        // ... End
    }
}
