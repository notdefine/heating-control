<?php

namespace HeatingControl\Controller;

use HeatingControl\HeatingException;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\SensorStoreInterface;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

class CliStoreController
{

    /** @var SensorStoreInterface[] */
    protected $sensorStores;

    /** @var SensorStateService */
    protected $sensorService;

    public function __construct(SensorStateService $sensorService, SensorStoreInterface ...$sensorStores)
    {
        $this->sensorService = $sensorService;
        $this->sensorStores = $sensorStores;
    }

    public function showSensors(Request $request, Response $response, array $args)
    {
        dump($this->sensorService->getAllSensorValues());
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return int|Response
     * @throws HeatingException
     */
    public function logSensors(Request $request, Response $response, array $args)
    {
        $allSensors = $this->sensorService->getAllSensorValues();
        foreach ($this->sensorStores as $sensorStore) {
            if (false === $sensorStore->storeValues(... array_values($allSensors))
            ) {
                return $response->getBody()->write('ERROR');
            }
        }
        return $response->withJson('Values stored');
    }
}
