<?php
/**
 * @author Thomas Eimers <notdefine@gmx.de>
 */

namespace HeatingControl\Controller;

use HeatingControl\HeatingException;
use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\RelayService;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

class RelayController
{
    /** @var MessageQueueService */
    protected $messageService;

    /** @var RelayService */
    protected $relayService;

    /**
     * @param RelayService $relayService
     * @param MessageQueueService $messageService
     */
    public function __construct(RelayService $relayService, MessageQueueService $messageService)
    {
        $this->relayService = $relayService;
        $this->messageService = $messageService;
    }

    public function index(Request $request, Response $response, array $args)
    {
        $result = [];
        foreach (RelayService::GPIO_PORTS as $gpio) {
            $result[$gpio] = $this->relayService->getRelay($gpio);
        }
        return $response->withJson($result);
    }

    public function gpio(Request $request, Response $response, array $args)
    {
        $gpio = mb_strtolower($args['gpio']);
        if ($request->getMethod() === 'GET') {
            $response->getBody()->write("Controler: $gpio has status " . $this->relayService->getRelay($gpio));
        }
        if ($request->getMethod() === 'POST') {
            $allPostPutVars = (array)$request->getParsedBody();
            if (!isset($allPostPutVars['status'])) {
                throw new HeatingException('Missing status to set');
            }
            $newStatus = $allPostPutVars['status'];
            $this->relayService->setRelay($gpio, $newStatus);
            $this->messageService->publishSwitch($gpio, (int)$newStatus);
            $response->getBody()->write("Controler: $gpio set to " . $newStatus);
        }

        return $response;
    }
}
