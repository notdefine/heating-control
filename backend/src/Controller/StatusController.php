<?php
/**
 * @author Thomas Eimers <notdefine@gmx.de>
 */

namespace HeatingControl\Controller;

use HeatingControl\Container\Combine;
use HeatingControl\Service\HeatingManagementService;
use HeatingControl\Service\RelayService;
use HeatingControl\Service\TemperatureMappingService;
use Slim\Container;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

class StatusController
{
    /** @var Container Dieser Controller darf direkt auf alle Ressourcen zugreifen, da er einen overall Status */
    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function status(Request $request, Response $response, array $args): Response
    {
        /** @var HeatingManagementService $burnerService */
        $burnerService = $this->container[HeatingManagementService::class];

        /** @var TemperatureMappingService $mappingService */
        $mappingService = $this->container[TemperatureMappingService::class];

        /** @var RelayService $relayService */
        $relayService = $this->container[RelayService::class];

        $status['status'] = [
            'burnerStatus' => $burnerService->getLastBurnerStatus(),
            'outsideTemp' => $burnerService->getCurrentOutsideTemp(),
            'returnFlowTemp' => $burnerService->getCurrentReturnFlowTemp(),
            'returnFlowTarget' => $burnerService->getCalculatetTargetReturnFlowTemp(),
        ];

        $status['relais'] = $relayService->getAllRelays();
        $status['config'] = $this->container[Combine::CONFIG];
        $status['temperatureMappings'] = $mappingService->getAllHumanReadable();

        return $response->withJson($status);
    }
}
