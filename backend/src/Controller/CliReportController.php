<?php

namespace HeatingControl\Controller;

use HeatingControl\Service\RrdService;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

class CliReportController
{

    /** @var  RrdService */
    protected $rrdService;

    public function __construct(RrdService $rrdService)
    {
        $this->rrdService = $rrdService;
    }

    public function graph(Request $request, Response $response, array $args): Response
    {
        $this->rrdService->generateImage();
        return $response->withJson('Image temp.svg/temp-week.svg generated');
    }
}
