<?php
/**
 * @author Thomas Eimers <notdefine@gmx.de>
 */

namespace HeatingControl\Controller;

use HeatingControl\HeatingException;
use HeatingControl\Service\SensorStateService;
use HeatingControl\State\State;
use Slim\Http\Request as Request;
use Slim\Http\Response as Response;

class SensorsValueController
{
    /** @var SensorStateService */
    protected $sensorService;

    public function __construct(SensorStateService $sensorService)
    {
        $this->sensorService = $sensorService;
    }

    public function identification(Request $request, Response $response, array $args): Response
    {
        $sensorIdentification = $args['identification'];

        try {
            $sensorValue = $this->sensorService->getSensorValueBySensorIdentification($sensorIdentification);
        } catch (HeatingException $e) {
            return $response->withJson(['error' => $e->getMessage()])
                ->withStatus(400, 'Could not get Sensor Values');
        }
        return $response->withJson($sensorValue->extract());
    }

    public function name(Request $request, Response $response, array $args)
    {
        $sensorName = $args['name'];
        try {
            $sensorValue = $this->sensorService->getSensorValueBySensorName($sensorName);
        } catch (HeatingException $e) {
            return $response->withJson(['error' => $e->getMessage()])
                ->withStatus(400, 'Could not get Sensor Values');
        }
        return $response->withJson($sensorValue->extract());
    }


    public function all(Request $request, Response $response, array $args)
    {
        try {
            $sensorValues = $this->sensorService->getAllSensorValues();
        } catch (HeatingException $e) {
            return $response->withJson(['error' => $e->getMessage()])->withStatus(400, 'Could not get Sensor Values');
        }

        /** @var State $sensorValue */
        foreach ($sensorValues as $key => $sensorValue) {
            $sensorValues[$key] = $sensorValue->extract();
        }

        return $response->withJson($sensorValues);
    }
}
