<?php

namespace HeatingControl\Controller;

use League\CommonMark\CommonMarkConverter;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Http\Request as Request;
use Zend\Diactoros\Stream;

class ApiDocumentationController
{
    public function index(Request $request, Response $response, array $args)
    {
        $converter = new CommonMarkConverter();
        $body = new Stream('php://temp', 'w');
        $body->write(
            $converter
                ->convertToHtml(
                    file_get_contents(__DIR__ . '/../../../docs/ApiDocumentation.md') . file_get_contents(__DIR__ . '/../../../docs/cli-commands.md')
                )
        );
        return $response->withBody($body);
    }
}
