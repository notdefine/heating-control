<?php

namespace HeatingControl\Container;

use HeatingControl\Controller\CliStoreController;
use HeatingControl\Controller\CliReportController;
use HeatingControl\Service\RrdService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\SensorStoreLogger;
use HeatingControl\Service\SensorStoreMemcached;
use HeatingControl\Service\SensorStoreMqqt;
use HeatingControl\Service\SensorStoreRrd;
use Psr\Container\ContainerInterface;
use Slim\Container;

class CliConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[CliStoreController::class] = function (Container $container): CliStoreController {
            return new CliStoreController(
                $container[SensorStateService::class],
                $container[SensorStoreMemcached::class],
                $container[SensorStoreRrd::class],
                $container[SensorStoreMqqt::class],
                $container[SensorStoreLogger::class]
            );
        };

        $container[CliReportController::class] = function (Container $container): CliReportController {
            return new CliReportController(
                $container[RrdService::class]
            );
        };

        return $container;
    }
}
