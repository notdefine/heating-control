<?php

namespace HeatingControl\Container;

use DateTime;
use Doctrine\ORM\EntityManager;
use HeatingControl\Entity\TemperatureMappingEntity;
use HeatingControl\HeatingException;
use HeatingControl\Service\HeatingManagementService;
use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\NightSettingsService;
use HeatingControl\Service\RelayService;
use HeatingControl\Service\RrdService;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\ShellCommandService;
use HeatingControl\Service\StateCacheService;
use HeatingControl\Service\TemperatureMappingService;
use HeatingControl\Service\TrackSensorsService;
use Memcached;
use Mosquitto\Client;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Container;

class ServiceConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[SensorSettingService::class] = function (Container $container): SensorSettingService {
            return new SensorSettingService($container->get(EntityManager::class));
        };

        $container[SensorStateService::class] = function (Container $container): SensorStateService {
            return new SensorStateService(
                $container[ShellCommandService::class],
                $container[SensorSettingService::class],
                $container[StateCacheService::class],
                $container[Combine::CONFIG]['heating']
            );
        };

        $container[ShellCommandService::class] = function (Container $container): ShellCommandService {
            return new ShellCommandService($container[LoggerInterface::class]);
        };

        $container[RrdService::class] = function (Container $container): RrdService {
            return new RrdService(
                $container[ShellCommandService::class],
                $container[SensorStateService::class],
                $container[Combine::CONFIG]['heating']
            );
        };

        $container[Client::class] = function (Container $container): Client {
            $conf = $container[Combine::CONFIG]['mosquitto'];
            $client = new Client();

            $connectStatus = $client->connect($conf['host'], $conf['port']);

            if ($connectStatus !== 0) {
                throw new HeatingException('Could not connect to MQQT' . $connectStatus);
            }

            $client->loop();
            return $client;
        };


        $container[RelayService::class] = function (Container $container): RelayService {
            return new RelayService($container[ShellCommandService::class]);
        };

        $container[MessageQueueService::class] = function (Container $container): MessageQueueService {
            return new MessageQueueService(
                $container[Combine::CONFIG]['mosquitto']
            );
        };

        $container[NightSettingsService::class] = function (Container $container): NightSettingsService {
            return new NightSettingsService($container[Combine::CONFIG]['heating'], new DateTime());
        };

        $container[HeatingManagementService::class] = function (Container $container): HeatingManagementService {
            return new HeatingManagementService(
                $container[Combine::CONFIG]['heating'],
                $container[SensorStateService::class],
                $container[RelayService::class],
                $container[NightSettingsService::class],
                $container[TemperatureMappingService::class],
                $container[StateCacheService::class],
                $container[LoggerInterface::class]
            );
        };

        $container[TemperatureMappingService::class] = function (Container $container): TemperatureMappingService {
            return new TemperatureMappingService(
                $container[Combine::CONFIG]['heating'],
                $container[EntityManager::class]->getRepository(TemperatureMappingEntity::class)
            );
        };

        $container[StateCacheService::class] = function (Container $container): StateCacheService {
            $memcached = new Memcached();
            $memcached->addServer("127.0.0.1", 11211);

            return new StateCacheService($memcached);
        };


        return $container;
    }
}
