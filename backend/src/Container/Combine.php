<?php

namespace HeatingControl\Container;

use Psr\Container\ContainerInterface;

class Combine implements CombineInterface
{
    const CONFIG = 'AppConfig';

    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container = CliConfig::init($container);
        $container = DatabaseConfig::init($container);
        $container = ControllerConfig::init($container);
        $container = ServiceConfig::init($container);
        $container = LoggerConfig::init($container);
        $container = SensorStoreConfig::init($container);

        $container[self::CONFIG] = function () {
            return [
                'heating' => require __DIR__ . '/../../config/heating.php',
                'mosquitto' => require __DIR__ . '/../../config/mosquitto.php',
                'database' => require __DIR__ . '/../../config/database.php'
            ];
        };

        return $container;
    }
}
