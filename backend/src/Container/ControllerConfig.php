<?php

namespace HeatingControl\Container;

use HeatingControl\Controller\BurnerController;
use HeatingControl\Controller\CliQueueWatcherController;
use HeatingControl\Controller\RelayController;
use HeatingControl\Controller\SensorSettingController;
use HeatingControl\Controller\SensorsValueController;
use HeatingControl\Service\HeatingManagementService;
use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\RelayService;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\StateCacheService;
use Psr\Container\ContainerInterface;
use Slim\Container;

class ControllerConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[SensorsValueController::class] = function (Container $container): SensorsValueController {
            return new SensorsValueController($container[SensorStateService::class]);
        };

        $container[SensorSettingController::class] = function (Container $container): SensorSettingController {
            return new SensorSettingController($container->get(SensorSettingService::class));
        };

        $container[RelayController::class] = function (ContainerInterface $container): RelayController {
            return new RelayController(
                $container->get(RelayService::class),
                $container->get(MessageQueueService::class)
            );
        };

        $container[CliQueueWatcherController::class] = function (Container $container): CliQueueWatcherController {
            return new CliQueueWatcherController(
                $container->get(MessageQueueService::class),
                $container->get(StateCacheService::class)
            );
        };

        $container[BurnerController::class] = function (Container $container): BurnerController {
            return new BurnerController(
                $container->get(HeatingManagementService::class)
            );
        };

        return $container;
    }
}
