<?php

namespace HeatingControl\Container;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;
use Psr\Container\ContainerInterface;
use Slim\Container;

class DatabaseConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[EntityManager::class] = function (Container $container): EntityManager {
            $isDevMode = $container[Combine::CONFIG]['heating']['debug'];

            // the connection configuration
            $dbParams = $container[Combine::CONFIG]['database'];

            $paths = ["./src/Entity"];
            $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);

            // replace with mechanism to retrieve EntityManager in your app
            return EntityManager::create($dbParams, $config);
        };

        return $container;
    }
}
