<?php

namespace HeatingControl\Container;

use Psr\Container\ContainerInterface;

interface CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface;
}
