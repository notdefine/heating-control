<?php

namespace HeatingControl\Container;

use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\SensorStoreLogger;
use HeatingControl\Service\SensorStoreMemcached;
use HeatingControl\Service\SensorStoreMqqt;
use HeatingControl\Service\SensorStoreRrd;
use HeatingControl\Service\ShellCommandService;
use HeatingControl\Service\StateCacheService;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Container;

class SensorStoreConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[SensorStoreRrd::class] = function (Container $container): SensorStoreRrd {
            return new SensorStoreRrd(
                $container[ShellCommandService::class]
            );
        };

        $container[SensorStoreMqqt::class] = function (Container $container): SensorStoreMqqt {
            return new SensorStoreMqqt(
                $container[MessageQueueService::class],
                $container[LoggerInterface::class]
            );
        };

        $container[SensorStoreMemcached::class] = function (Container $container): SensorStoreMemcached {
            return new SensorStoreMemcached($container[StateCacheService::class]);
        };

        $container [SensorStoreLogger::class] = function (Container $container): SensorStoreLogger {
            return new SensorStoreLogger($container[LoggerInterface::class]);
        };


        return $container;
    }
}
