<?php

namespace HeatingControl\Container;

use Monolog\Handler\ErrorLogHandler;
use Monolog\Handler\SyslogHandler;
use Monolog\Logger as MonoLogger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Container;

class LoggerConfig implements CombineInterface
{
    public static function init(ContainerInterface $container): ContainerInterface
    {
        $container[LoggerInterface::class] = function (Container $container): LoggerInterface {
            $logger = new MonoLogger('heating');
            $logger->pushHandler(new SyslogHandler('heating', LOG_USER));
            if (PHP_SAPI === 'cli') {
                $logger->pushHandler(new ErrorLogHandler ());
            }
            return $logger;
        };

        return $container;
    }
}
