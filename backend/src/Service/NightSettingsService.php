<?php

namespace HeatingControl\Service;

use DateTime;

/**
 * Aktuell sind der MVP halber die Nachtabsenkungszeiten fest programmiert
 */
class NightSettingsService
{
    /** @var array */
    protected $appConfig;

    /** @var DateTime */
    protected $currentDate;

    public function __construct(array $appConfig, DateTime $currentDate)
    {
        $this->appConfig = $appConfig;
        $this->currentDate = $currentDate;
    }

    /**
     * Ist die Nachtabsenkung aktiv?
     *
     * @return bool
     */
    public function isNightlySinkActive()
    {
        $hour = $this->getCurrentHour();
        if ($hour < self::readSettingTemperatureSinkFrom()
            &&
            $hour >= self::readSettingTemperatureSinkTo()
        ) {
            #echo "NightlySinkNotActive";
            return false;
        }
        #echo "NightlySinkActive";
        return true;
    }

    public function getCurrentHour()
    {
        return $this->currentDate->format('G');
    }

    public function readSettingTemperatureSinkFrom()
    {
        return $this->appConfig['nightTimeStart'];
    }

    public function readSettingTemperatureSinkTo()
    {
        return $this->appConfig['nightTimeEnd'];
    }

    /**
     * Nachtabsenkung
     *
     * @return int
     */
    public function getNightlySubstract(): int
    {
        return $this->appConfig['nightlySubstract'];
    }
}
