<?php

namespace HeatingControl\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use HeatingControl\Entity\SensorSettingEntity;

/**
 * Get Sensor configuration Settings from Database
 */
class SensorSettingService
{
    protected $entityManager;

    // Eigentlich würde man hier ein Repository erwarten
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param string $name
     * @return SensorSettingEntity|false
     */
    public function getSettingByName($name)
    {
        $setting = $this->getSettings(['name' => $name]);
        if (empty($setting)) {
            return false;
        }

        return $setting[0];
    }

    /**
     * @param array $filter
     * @return SensorSettingEntity[]|false
     */
    public function getSettings(array $filter = [])
    {
        $entities = $this->entityManager->getRepository(SensorSettingEntity::class)->findBy($filter);

        if (empty($entities)) {
            return false;
        }
        return $entities;
    }

    /**
     * @param string $identification
     * @return SensorSettingEntity|false
     */
    public function getSettingByIdentification($identification)
    {
        $setting = $this->getSettings(['identification' => $identification]);
        if (empty($setting)) {
            return false;
        }
        return $setting[0];
    }

    /**
     * @param SensorSettingEntity $setting
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function setSetting(SensorSettingEntity $setting)
    {
        $this->entityManager->persist($setting);
        $this->entityManager->flush();
    }
}
