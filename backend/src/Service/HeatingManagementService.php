<?php

declare(strict_types=1);

namespace HeatingControl\Service;

use HeatingControl\HeatingException;
use HeatingControl\State\RelayState;
use HeatingControl\State\TemperatureSensorState;
use Psr\Log\LoggerInterface;

/**
 * Class HeatingManagementService
 * @package HeatingControl\Service
 *
 * The heart of this application. Get all pieces together and decide what to do with
 * the current temperature and burner.
 */
class HeatingManagementService
{
    const RELAY_CONFIG = 'relayIds';

    /** @var array */
    protected $config;

    /** @var SensorStateService */
    protected $sensorService;

    /** @var RelayService */
    protected $relayService;

    /** @var TemperatureMappingService */
    protected $mappingService;

    /** @var NightSettingsService */
    protected $nightSettingService;

    /** @var StateCacheService */
    protected $memcachedService;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(
        array $config,
        SensorStateService $sensorService,
        RelayService $relayService,
        NightSettingsService $nightSettingService,
        TemperatureMappingService $mappingService,
        StateCacheService $memcachedService,
        LoggerInterface $logger
    ) {
        $this->config = $config;
        $this->sensorService = $sensorService;
        $this->relayService = $relayService;
        $this->nightSettingService = $nightSettingService;
        $this->mappingService = $mappingService;
        $this->memcachedService = $memcachedService;
        $this->logger = $logger;
    }

    /**
     * Die Zentrale Steuerung, wird jede Minute einmal aufgerufen
     *
     * @throws HeatingException
     */
    public function updateStatus()
    {
        $targetReturnFlowTemp = $this->getCalculatetTargetReturnFlowTemp();
        // welchen status hat der brenner beim letzen mal gehabt?
        $lastBurnerStatus = $this->getLastBurnerStatus();
        //-----------------------------------------------------------------------------
        // Brenner je nach Wassertemperatur/Außentemperatur ein/ausschalten
        //-----------------------------------------------------------------------------
        $newBurnerStatus = $this->mustBurnerBeActive(
            $targetReturnFlowTemp,
            $this->getCurrentReturnFlowTemp()
        );
        $this->logger->info('Ist ' . $this->getCurrentReturnFlowTemp() . ' soll ' . $targetReturnFlowTemp . PHP_EOL);

        $this->logBurnerStatus($lastBurnerStatus);
        $this->logPumpStatus($this->getLastPumpStatus());

        // Aber nur regeln wenn die Heizungssteuerung aktiv ist
        if (!$this->isActive()) {
            $this->logger->warning("Automatische Steuerung ist inakiv");
        }
        // Wenn der Wert NULL ist, soll der brenner einfach im aktuellen zustand bleiben
        // das sind die zwei grad zwischen ein und auschalten, in dem er weiter aufheizen,
        // bzw weiter abkühlen soll, je nachdem was er gerade macht
        if (is_null($newBurnerStatus)) {
            if ($lastBurnerStatus) {
                $this->logger->info("Schwellwertbereich: Brenner bleibt ein");
                $this->turnBurnerOn();
            } else {
                $this->logger->info("Schwellwertbereich: Brenner bleibt aus");
                #TODO Pumpe muss trotzdem laufen
                $this->turnBurnerOff();
            }
        } elseif ($newBurnerStatus === true) {
            $this->turnBurnerOn();
        } elseif ($newBurnerStatus === false) {
            $this->turnBurnerOff();
        }
        $this->controlPump();
        $this->storeWarmwaterUsage();
    }

    /**
     * @return float
     * @throws HeatingException
     */
    public function getCalculatetTargetReturnFlowTemp()
    {
        return $this->getReturnFlowTempForOutsideTemp($this->getCurrentOutsideTemp());
    }

    /**
     * Rücklauftemperatur Soll in Abhängigkeit von
     * - der Aussentemperatur
     * - der Nachtabsenkung
     * - der Frostsicherung
     *
     * @param float $outsideTemp aktuelle Außentemperatur
     *
     * @return float Zieltemperatur für Rücklaufwasser
     * @throws HeatingException
     */
    public function getReturnFlowTempForOutsideTemp(float $outsideTemp): float
    {
        $destinationTemp = $this->mappingService->getReturnFlowValueForOutsideTemp($outsideTemp);

        // Nachtabsenkung beachten?
        if ($this->isNightlySinkActive()) {
            $destinationTemp -= $this->nightSettingService->getNightlySubstract();
        }

        // Keiner zu hause? Dann eco modus (von Home Assistent mitgeteilt)
        $ecoMode = $this->memcachedService->get('eco');
        $this->logger->info('Eco Mode' . ($ecoMode) ? 'ON' : 'OFF');

        if ($ecoMode) {
            $destinationTemp -= $this->config['eco-mode-sink'];
        }

        // Frostschutz
        return $this->getFrostProtectionTemperature($destinationTemp);
    }

    public function isNightlySinkActive()
    {
        return $this->nightSettingService->isNightlySinkActive();
    }

    /**
     * Sichert eine garantierte soll Rücklauftemperatur von 13 Grad, damit auch im Fehlerfall
     * es nicht friert
     *
     * @param float $temp Rücklauftemperatur (errechnet)
     *
     * @return float $temp Mindesttemperatur von 13Grad
     */
    protected function getFrostProtectionTemperature($temp): float
    {
        // Frostschutz
        return max($this->config['frostProtectionWaterTemperature'], $temp);
    }

    /**
     * @return float
     * @throws HeatingException
     */
    public function getCurrentOutsideTemp(): float
    {
        // try to get last sensor value from memcached, when not exist read sensor directly
        $cachedValue = $this->memcachedService->get($this->config['systemSensorsIds']['outside']);
        if ($cachedValue !== null) {
            return (float)$cachedValue;
        }

        /** @var TemperatureSensorState $sensor */
        $sensor = $this->sensorService->getSensorValueBySensorIdentification(
            $this->config['systemSensorsIds']['outside']
        );
        return (float)$sensor->getValue();
    }

    // Über setting steuerbar machen

    public function getLastBurnerStatus(): ?bool
    {
        return $this->memcachedService->get(RelayState::BURNER_KEY);
    }

    /**
     * Soll die Heizung bei der Aktuelle Rücklauftemperatur und der Aktuellen Außentemperatur
     * aktiviert werden
     *
     * @param float $targetReturnFlowTemp Welche Rücklauftemperatur in Abhängikeit von einer Außentemperatur ist gewünscht
     * @param float $currentReturnFlowTemp Aktuelle Rücklauftemperatur
     *
     * @return bool|NULL Heizung ein/ausschalten, oder einfach aktuellen Zustand so lassen=NULL (schmitt trigger)
     */
    public function mustBurnerBeActive($targetReturnFlowTemp, $currentReturnFlowTemp): ?bool
    {
        if ($currentReturnFlowTemp < $targetReturnFlowTemp - $this->config['threshold']) {
            return true;
        }

        if ($currentReturnFlowTemp > $targetReturnFlowTemp + $this->config['threshold']) {
            return false;
        }

        return null;
    }

    /**
     * @return float
     * @throws HeatingException
     */
    public function getCurrentReturnFlowTemp(): float
    {
        // try to get last sensor value from memcached, when not exist read sensor directly
        $cachedValue = $this->memcachedService->get($this->config['systemSensorsIds']['returnFlow']);
        if ($cachedValue !== null) {
            return (float)$cachedValue;
        }

        /** @var TemperatureSensorState $sensor */
        $sensor = $this->sensorService->getSensorValueBySensorIdentification(
            $this->config['systemSensorsIds']['returnFlow']
        );
        return (float)$sensor->getValue();
    }

    protected function logBurnerStatus($status)
    {
        switch ($status) {
            case true:
                $this->logger->info("Brenner war an");
                break;
            case false:
                $this->logger->info("Brenner war aus");
                break;
            default:
                $this->logger->warning("Brenner hat keinen Status");
        }
    }

    public function logPumpStatus($status)
    {
        switch ($status) {
            case true:
                $this->logger->info("Pumpe war an");
                break;
            case false:
                $this->logger->info("Pumpe war aus");
                break;
            default:
                $this->logger->warning("Pumpe hat keinen Status");
        }
    }

    public function getLastPumpStatus(): ?bool
    {
        return $this->memcachedService->get(RelayState::PUMP_KEY);
    }

    public static function isActive()
    {
        return true;
    }

    public function turnBurnerOn()
    {
        $this->logger->info("Brenner wird EIN geschaltet");
        $this->saveLastBurnerStatus(true);
        $this->relayService->setRelay(
            $this->config[self::RELAY_CONFIG][RelayState::BURNER_KEY],
            RelayService::IO_VALUE_ON
        );
    }

    public function saveLastBurnerStatus(bool $status)
    {
        $this->memcachedService->set(RelayState::BURNER_KEY, $status);
    }

    public function turnBurnerOff()
    {
        $this->logger->info("Brenner wird AUS geschaltet");
        $this->saveLastBurnerStatus(false);
        $this->relayService->setRelay($this->config[self::RELAY_CONFIG][RelayState::BURNER_KEY], RelayService::IO_VALUE_OFF);
    }

    public function controlPump()
    {
        if ($this->getLastBurnerStatus()) {
            $this->turnPumpOn();
            return;
        }

        if ($this->isNightlySinkActive()) {
            $this->turnPumpOff();
            return;
        }
        $this->turnPumpOn();

        /*

        //-----------------------------------------------------------------------------
        // Pumpe X minuten nachlaufen lassen
        //-----------------------------------------------------------------------------
        $diff_seconds = $this->getLastBurnerRun();
        echo "Last Burner activity " . $diff_seconds . ' Seconds ' . PHP_EOL;

        // Pumpe nur regeln wenn heizungssteuerung Aktiv ist
        if ($this->isActive()) {
            if ($this->PumpAvoidStuck($diff_seconds)) {
                echo "Pumpe ein, damit sich die Flügel im Sommer nicht festsetzen" . PHP_EOL;
                $this->setPumpActive();
            } else {
                if ($this->PumpFollowUp($diff_seconds)) {
                    echo "Pumpe ein" . PHP_EOL;
                    // Weniger als X minuten, also laufen Pumpen noch
                    $this->setPumpActive();
                } else {
                    echo "Pumpe aus" . PHP_EOL;
                    $this->setPumpInactive();
                }
            }
        } else {
            // nicht die Manuelle Steuerung überschreiben
            // $this->setPumpInactive();
        }
        */
    }

    // Mal gucken wo ich das speicher

    public function turnPumpOn()
    {
        $this->logger->info("Pumpe wird EIN geschaltet");
        $this->saveLastPumpStatus(true);
        $this->relayService->setRelay($this->config[self::RELAY_CONFIG][RelayState::PUMP_KEY], RelayService::IO_VALUE_ON);
    }

    public function saveLastPumpStatus(bool $status)
    {
        $this->memcachedService->set(RelayState::PUMP_KEY, $status);
    }

    public function storeWarmwaterUsage()
    {
        /*
            //-----------------------------------------------------------------------------
            // Warmwasserverbrauch mitloggen
            //-----------------------------------------------------------------------------
            $water_use = $this->isHotWaterActive();

            $sensor = R::dispense('sensor');

            $sensor->name = 'warm_water';
            $sensor->value = $water_use;
            $sensor->timestamp = date('c');
            R::store($sensor);
        */
    }

    public function turnPumpOff()
    {
        $this->logger->info("Pumpe wird AUS geschaltet");
        $this->saveLastPumpStatus(false);
        $this->relayService->setRelay($this->config[self::RELAY_CONFIG][RelayState::PUMP_KEY], RelayService::IO_VALUE_OFF);
    }
}
