<?php
declare(strict_types=1);

namespace HeatingControl\Service;

use HeatingControl\HeatingException;

class RelayService
{
    const IO_VALUE_ON = '1';
    const IO_VALUE_OFF = '0';

    const CONFIG_KEY = 'relayIds';

    // https://www.waveshare.com/wiki/RPi_Relay_Board#Interface_description see BCM
    const GPIO_PORTS = [
        'gpio26',
        'gpio20',
        'gpio21'
    ];

    /** @var ShellCommandService */
    protected $commandService;

    public function __construct(ShellCommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    /**
     * @return array
     * @throws HeatingException
     */
    public function getAllRelays(): array
    {
        $relayStatus = [];
        foreach (self::GPIO_PORTS as $gpio) {
            $relayStatus[$gpio] = $this->getRelay($gpio);
        }
        return $relayStatus;
    }

    /**
     * @param string $gpio
     * @return string
     * @throws HeatingException
     */
    public function getRelay(string $gpio): string
    {
        $this->isGpioValid($gpio);
        $status = $this->commandService->exec('cat /sys/class/gpio/' . escapeshellcmd($gpio) . '/value');
        // Current GPIO Setting represents the "real" Status of the switch, so update always the MessageQueue
        return trim($status);
    }

    /**
     * @param string $gpio
     * @throws HeatingException
     */
    protected function isGpioValid(string $gpio): void
    {
        if (!in_array($gpio, RelayService::GPIO_PORTS)) {
            throw new HeatingException('Only Support Waveshare Raspberry Pi Expansion Board, Power Relay');
        }
    }

    /**
     * @param string $gpio
     * @param string $newStatus self::IO_VALUE_ON, self::IO_VALUE_OFF
     * @return bool
     * @throws HeatingException
     */
    public function setRelay(string $gpio, string $newStatus)
    {
        $this->isGpioValid($gpio);
        if (!in_array($newStatus, [self::IO_VALUE_ON, self::IO_VALUE_OFF])) {
            throw new HeatingException('Invalid Relay Status');
        }

        $cmd = 'echo ' . escapeshellarg((string)$newStatus) . ' > /sys/class/gpio/' . escapeshellcmd($gpio) . '/value';
        $status = $this->commandService->exec(
            $cmd
        );
        return ($status !== null);
    }
}
