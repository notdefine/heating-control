<?php

namespace HeatingControl\Service;

use Psr\Log\LoggerInterface;

/**
 * Get access to RaspiCommandline
 */
class ShellCommandService
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $cmd
     * @return string
     */
    public function exec(string $cmd): ?string
    {
        $this->logger->debug($cmd);
        return shell_exec($cmd);
    }
}
