<?php

namespace HeatingControl\Service;

use HeatingControl\HeatingException;
use HeatingControl\State\State;
use HeatingControl\State\StoreRrdInterface;

class SensorStoreRrd implements SensorStoreInterface
{
    protected $commandService;

    public function __construct(ShellCommandService $commandService)
    {
        $this->commandService = $commandService;
    }

    public function storeValues(State ...$sensorValues)
    {
        $cmd = $this->getUpdateCommand(... $sensorValues);
        $returnVar = $this->commandService->exec($cmd . ' 2>&1');
        if (!is_null($returnVar)) {
            throw new HeatingException('Error saving Sensor Values ' . serialize($returnVar));
        }
    }

    /**
     *
     * Generate Command to update the RRD Database
     *
     * Try to use Sensor->id for assigning Sensors to the RRD tool
     *
     * @param State ...$sensorValues
     * @return string
     * @throws HeatingException
     */
    public function getUpdateCommand(State ...$sensorValues)
    {
        $cmd = 'rrdtool update /home/pi/.temperature/temp.rrd N:';

        // You can tell RRDtool a data source is unknown by giving it "U" as its value.
        $values = array_fill(1, 10, 'U');

        foreach ($sensorValues as $sensorValue) {
            /** @var StoreRrdInterface $sensorValue */
            if (!is_a($sensorValue, StoreRrdInterface::class)) {
                continue; // Save as in RRDService TODO
            }
            if ($sensorValue->getId() > 10) {
                throw new HeatingException('Sensor ID abvoe 10 not allowed. See sensor_settings table');
            }
            $values[$sensorValue->getId()] = $sensorValue->getRrdValue();
        }

        // rrdtool update /home/pi/.temperature/temp.rrd N:42.75:54.625:13.812:22.812:41.625:28.562:U:U:U:U
        return $cmd . implode(':', $values);
    }
}
