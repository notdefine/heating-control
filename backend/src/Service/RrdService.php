<?php


namespace HeatingControl\Service;

use DateTime;
use DateTimeInterface;
use HeatingControl\HeatingException;
use HeatingControl\State\State;
use HeatingControl\State\StoreRrdInterface;

class RrdService
{
    protected $commandService;
    protected $sensorService;

    protected $appConfig;

    public function __construct(ShellCommandService $commandService, SensorStateService $sensorService, array $appConfig)
    {
        $this->commandService = $commandService;
        $this->sensorService = $sensorService;
        $this->appConfig = $appConfig;
    }

    /**
     * @throws HeatingException
     */
    public function generateImage()
    {
        $sensorValues = $this->sensorService->getAllSensorValues();
        $cmd = $this->getImageCommand(new DateTime(), ...array_values($sensorValues));
        $result = $this->commandService->exec($cmd);
        dump($result);
        $cmd = $this->getImageCommandWeekly(new DateTime(), ...array_values($sensorValues));
        $result = $this->commandService->exec($cmd);
        dump($result);
    }

    /**
     * @param DateTime $date
     * @param State ...$sensorValues
     * @return string
     * @throws HeatingException
     */
    public function getImageCommand(DateTimeInterface $date, State ...$sensorValues): string
    {
        $sensorValues = $this->filterSensors(...array_values($sensorValues));
        if (empty($sensorValues)) {
            throw new HeatingException('No Sensorvalues to show');
        }

        $cmd = 'rrdtool graph /home/pi/heating-control/backend/public/images/temp.svg -a SVG --title="Temperatur 36h ' . $date->format('d.m.Y G:i') . '" --start -129600 --vertical-label "Grad Celsius" ';
        $cmd .= $this->getLines(...$sensorValues);
        return $cmd . "'GPRINT:probe" . array_pop($sensorValues)->getId() . ":LAST:Tempsensor Last Temp\: %2.1lf' " . '--font WATERMARK:0.1:DejaVuSansMono --font UNIT:0:DejaVuSansMono --font AXIS:6:DejaVuSansMono --font LEGEND:8:. -w 1000 -h350 --full-size-mode --x-grid HOUR:1:HOUR:2:HOUR:2:86400:%H:00 -E';
    }

    /**
     * @param State ...$sensorValues
     * @return string
     */
    protected function getLines(State ...$sensorValues): string
    {
        $sensorValues = $this->filterSensors(...array_values($sensorValues));

        $cmd = ' ';
        foreach ($sensorValues as $sensor) {
            $sensorId = $sensor->getId();
            $sensorName = $sensor->getName();

            # "'DEF:probe2=/home/pi/.temperature/temp.rrd:sensor2:AVERAGE' 'LINE1:probe2#ff0000:Sensor 2' " .
            $cmd .= "'DEF:probe" . $sensorId . "=/home/pi/.temperature/temp.rrd:sensor" . $sensorId . ":AVERAGE' 'LINE1:probe" . $sensorId . $this->getSensorColor($sensorId) . ":" . $sensorName . "' ";
        }
        return $cmd;
    }

    protected function getSensorColor(int $sensorNo): string
    {
        return $this->appConfig['diagrammColors'][$sensorNo % 10];
    }

    /**
     * @param DateTime $date
     * @param State ...$sensorValues
     * @return string
     * @throws HeatingException
     */
    public function getImageCommandWeekly(DateTimeInterface $date, ...$sensorValues): string
    {
        $cmd = 'rrdtool graph /home/pi/heating-control/backend/public/images/temp-week.svg -a SVG --title="Temperatur 8 Tage ' . $date->format('d.m.Y G:i') . '" --start -' . (60 * 60 * 24 * 8) . ' --vertical-label "Grad Celsius" ';

        $sensorValues = $this->filterSensors(...$sensorValues);
        $cmd .= $this->getLines(...$sensorValues);

        return $cmd . '--font WATERMARK:0.1:DejaVuSansMono --font UNIT:0:DejaVuSansMono --font AXIS:6:DejaVuSansMono --font LEGEND:8:. -w 1000 -h350 --full-size-mode -E';
    }

    private function filterSensors(State ...$sensorValues)
    {
        foreach ($sensorValues as $key => $sensorValue) {
            if (in_array($sensorValue->getName(), $this->appConfig['diagrammWeekly']['skipSensors'])) {
                echo "Skipped sensor " . $sensorValue->getName();
                unset($sensorValues[$key]);
            }
            /** @var StoreRrdInterface $sensorValue */
            if (!is_a($sensorValue, StoreRrdInterface::class)) {
                unset($sensorValues[$key]);
            }
        }
        return $sensorValues;
    }
}
