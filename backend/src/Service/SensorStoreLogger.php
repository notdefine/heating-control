<?php


namespace HeatingControl\Service;

use HeatingControl\State\State;
use Psr\Log\LoggerInterface;

/**
 * Write Sensor Values to Logger
 */
class SensorStoreLogger implements SensorStoreInterface
{
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function storeValues(State ...$sensorValues)
    {
        foreach ($sensorValues as $sensor) {
            $this->logger->debug('Read "' . $sensor->getName() . '" ' . $sensor->getValue());
        }
    }
}
