<?php

namespace HeatingControl\Service;

use HeatingControl\State\State;
use HeatingControl\State\StoreQueueInterface;
use Mosquitto\Client;
use Psr\Log\LoggerInterface;

/**
 * Read all sensors and write the current Values to the message Queue
 *
 * https://www.home-assistant.io/docs/mqtt/discovery/
 */
class SensorStoreMqqt implements SensorStoreInterface
{
    /** @var MessageQueueService */
    protected $messageQueueService;

    /** @var LoggerInterface $logger */
    protected $logger;

    public function __construct(MessageQueueService $messageQueueService, LoggerInterface $logger)
    {
        $this->messageQueueService = $messageQueueService;
        $this->logger = $logger;
    }

    /**
     * @param State ...$sensorValues
     */
    public function storeValues(State ...$sensorValues)
    {
        foreach ($sensorValues as $sensorValue) {
            if (!is_a($sensorValue, StoreQueueInterface::class)) {
                continue;
            }
            /** @var StoreQueueInterface $sensorValue */
            $sensorValue->publishQueueDiscovery($this->messageQueueService);
            $sensorStateUrl = $sensorValue->publishQueueValue($this->messageQueueService);

            $this->logger->debug('Stored "' . $sensorValue->getName() . '" in mqqt ' . $sensorStateUrl . ' ' . $sensorValue->getValue());
        }
    }
}
