<?php


namespace HeatingControl\Service;

use HeatingControl\State\State;

/**
 * Read all sensors and write the current Values in Database
 */
class SensorStoreMemcached implements SensorStoreInterface
{
    /** @var StateCacheService */
    protected $memcachedService;

    public function __construct(StateCacheService $memcachedService)
    {
        $this->memcachedService = $memcachedService;
    }

    /**
     * @param State ...$sensorValues
     */
    public function storeValues(State ...$sensorValues)
    {
        foreach ($sensorValues as $sensor) {
            $this->memcachedService->set($sensor->getIdentification(), $sensor->getValue());
        }
    }
}
