<?php


namespace HeatingControl\Service;

use Doctrine\ORM\EntityManager;
use HeatingControl\State\State;

/**
 * Read all sensors and write the current Values in Database
 */
class SensorStoreDatabase implements SensorStoreInterface
{
    protected $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param State ...$sensorValues
     * @todo implement when needed
     */
    public function storeValues(State ...$sensorValues)
    {

        // To DB
        echo "TODO Save DB";
    }
}
