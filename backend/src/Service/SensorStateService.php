<?php

namespace HeatingControl\Service;

use BadMethodCallException;
use HeatingControl\HeatingException;
use HeatingControl\State\RelayState;
use HeatingControl\State\State;
use HeatingControl\State\SwitchState;
use HeatingControl\State\TemperatureSensorState;

/**
 * Get access to sensors by using SensorSettings and using the 1-wire sensors
 *
 * Sensors have no configuration
 */
class SensorStateService
{
    const SYS_DIR = '/sys/bus/w1/devices/w1_bus_master1/';
    const FILTER = '/w1_slave | grep t= | cut -d\'=\' -f 2';
    /** @var ShellCommandService */
    protected $commandService;
    protected $sensorSettingService;
    protected $stateCacheService;
    protected $appConfig;

    public function __construct(ShellCommandService $commandService, SensorSettingService $settingsService, StateCacheService $stateCacheService, array $appConfig)
    {
        $this->commandService = $commandService;
        $this->sensorSettingService = $settingsService;
        $this->stateCacheService = $stateCacheService;
        $this->appConfig = $appConfig;
    }

    /**
     * Read all Sensors and read the optional Settings like his name. If it is not configured set an automated
     * Name. The result set also include Relays status
     *
     * @return State[]
     * @throws HeatingException
     */
    public function getAllSensorValues(): array
    {
        $sensorValues = [];

        foreach ($this->getAllConnectedSensors() as $sensorIdentification) {
            $sensorValues[$sensorIdentification] = $this->getSensorValueBySensorIdentification($sensorIdentification);
        }

        /** @var int[] $emptySensorIds */
        $emptySensorIds = [];

        // We are support maximal 10 Sensors. Sensors with configured ID's use they configured number
        // Sensors without a configuration are stored at the end of the list, but 9 and 10 are reserved
        // for the relay status.
        foreach (range(1, 8, 1) as $number) {
            $emptySensorIds[$number] = $number;
        }
        foreach ($sensorValues as $sensor) {
            unset($emptySensorIds[$sensor->getId()]);
        }

        $burnerStatus = new RelayState();
        $burnerStatus->setValue(
            $this->stateCacheService->get(RelayState::BURNER_KEY) ? "1" : "0"
        );
        $burnerStatus->setName(RelayState::BURNER_KEY);
        $burnerStatus->setIdentification($this->appConfig[RelayService::CONFIG_KEY][RelayState::BURNER_KEY]);
        $burnerStatus->setId(RelayState::DIAGRAM_BURNER_ID);

        $pumpStatus = new RelayState();
        $pumpStatus->setValue(
            $this->stateCacheService->get(RelayState::PUMP_KEY) ? "1" : "0"
        );
        $pumpStatus->setName(RelayState::PUMP_KEY);
        $pumpStatus->setIdentification($this->appConfig[RelayService::CONFIG_KEY][RelayState::PUMP_KEY]);
        $pumpStatus->setId(RelayState::DIAGRAM_PUMP_ID);

        $ecoStatus = new SwitchState();
        $ecoStatus->setValue((string)0);
        $ecoStatus->setName('Eco Mode');
        $ecoStatus->setIdentification(SwitchState::ECO_KEY);
        $ecoStatus->setId(11);

        $sensorValues[] = $burnerStatus;
        $sensorValues[] = $pumpStatus;
        $sensorValues[] = $ecoStatus;

        // Set name and ID for unconfigured Sensors
        /** @var State $sensor */
        foreach ($sensorValues as &$sensor) {
            if (empty($sensor->getName())) {
                $sensor->setId(array_pop($emptySensorIds));
                $sensor->setName('Sensor ' . $sensor->getId());
                $sensor->setConfigured(false);
            } else {
                $sensor->setConfigured(true);
            }
        }
        return $sensorValues;
    }


    /**
     * Get all connected temperatur sensors. They always start with "28"
     *
     * @return array
     */
    protected function getAllConnectedSensors(): array
    {
        $result = $this->commandService->exec('cat ' . self::SYS_DIR . '[12][0-9]-*/name');
        $result = explode(PHP_EOL, $result);

        $sensors = [];
        foreach ($result as $sensorName) {
            if (empty($sensorName)) {
                continue;
            }
            $sensors[$sensorName] = $sensorName;
        }

        return $sensors;
    }

    /**
     * @param string $sensorIdentification For example "28-000008fc6545"
     * @param string $sensorName (optional)
     * @return State
     * @throws HeatingException
     */
    public function getSensorValueBySensorIdentification($sensorIdentification, $sensorName = null): State
    {
        // 24375\n
        $result = $this->commandService->exec('cat ' . self::SYS_DIR . escapeshellarg($sensorIdentification) . self::FILTER);

        if (empty($result)) {
            throw new HeatingException('Sensor ' . $sensorIdentification . ' is not readable');
        }

        $result = trim($result);

        $entity = new TemperatureSensorState();
        $entity->setValue((string)($result / 1000));
        $entity->setIdentification($sensorIdentification);

        if (empty($sensorName)) {
            // When sensor is known to settings, add the sensor name
            $setting = $this->sensorSettingService->getSettingByIdentification($sensorIdentification);
            if (!empty($setting)) {
                $entity->setName($setting->getName());
                $entity->setId($setting->getId());
            }
            return $entity;
        }

        $entity->setName($sensorName);
        return $entity;
    }

    /**
     * Maps Name to identification, and read sensor by id
     *
     * @param string $name
     * @return State
     * @throws HeatingException
     */
    public function getSensorValueBySensorName(string $name): State
    {
        $setting = $this->sensorSettingService->getSettingByName($name);
        if ($setting === false) {
            throw new BadMethodCallException("Sensor $name does not exits");
        }

        return $this->getSensorValueBySensorIdentification($setting->getIdentification(), $name);
    }
}
