<?php

namespace HeatingControl\Service;

use HeatingControl\State\State;

interface SensorStoreInterface
{
    public function storeValues(State ...$sensorValues);
}
