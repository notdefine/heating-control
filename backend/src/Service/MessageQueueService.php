<?php


namespace HeatingControl\Service;

use HeatingControl\HeatingException;
use Mosquitto\Client;
use Mosquitto\Message;

class MessageQueueService
{
    const MESSAGE_PREFIX_SWITCH = 'myhome/switch/';

    protected $mosquitto;

    public function __construct($config)
    {
        $this->mosquitto = $this->getMessageQueue($config);
    }

    protected function getMessageQueue(array $config): Client
    {
        $client = new Client();
        $connectStatus = $client->connect($config['host'], $config['port']);
        if ($connectStatus !== 0) {
            throw new HeatingException('Connection to Mqtt mosquitto failed:' . $connectStatus);
        }

        $client->subscribe('#', 1); // Subscribe to all messages
        $client->loop();
        return $client;
    }

    public function publishSwitch($gpio, $status)
    {
        $this->publish(MessageQueueService::MESSAGE_PREFIX_SWITCH . $gpio, (int)$status);
    }

    public function publish(string $item, $value)
    {
        $this->mosquitto->publish($item, $value, 2, false);
        $this->mosquitto->loop();
    }

    public function onMessage($callback)
    {
        $this->mosquitto->onMessage($callback);
        $this->mosquitto->loop();
    }

    public function loopForever()
    {
        $this->mosquitto->loopForever();
    }

    public function __destruct()
    {
        // Only when connection was established
        if (is_object($this->mosquitto)) {
            $this->mosquitto->loop();
            $this->mosquitto->disconnect();
        }
    }
}
