<?php

namespace HeatingControl\Service;

use Doctrine\ORM\EntityRepository;
use Exception;
use HeatingControl\Entity\TemperatureMappingEntity;

/**
 * Get Sensor configuration Settings from Database
 */
class TemperatureMappingService
{
    /** @var array */
    protected $config;


    /** @var EntityRepository */
    protected $mappingRepository;

    public function __construct(
        array $config,
        EntityRepository $mappingRepository
    ) {
        $this->config = $config;
        $this->mappingRepository = $mappingRepository;
    }

    /**
     * Gibt die vorgabe für Rücklaufwert zu einer Außentemperatur aus der Datenbankkonfiguration
     * zurück
     *
     * könnte auch ein eigener service sein, mal sehen
     *
     * @param float $outsideTemp
     *
     * @return float $destinationTemp
     */
    public function getReturnFlowValueForOutsideTemp(float $outsideTemp): float
    {
        try {
            $result = $this->mappingRepository->findBy(
                ['outside_temp' => round($outsideTemp)]
            );
            /** @var TemperatureMappingEntity $entity */
            $entity = array_shift($result);
            return $entity->getReturnFlowTemp();
        } catch (Exception $e) {
            // Es konnte kein Wert ermittelt werden, dann setzen wir "lauwarm" an
            return $this->config['missingMappingValueFallback'];
        }
    }

    public function getAllHumanReadable(): array
    {
        $mapping = [];
        foreach ($this->mappingRepository->findAll() as $map) {
            /** @var TemperatureMappingEntity $map */
            $mapping[$map->getId()] =
                'For outside temperature ' . $map->getOutsideTemp() . ' the target temperature is ' . $map->getReturnFlowTemp();
        }
        return $mapping;
    }
}
