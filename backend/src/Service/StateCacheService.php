<?php

namespace HeatingControl\Service;

use HeatingControl\HeatingException;
use Memcached;

/**
 * Get access to RaspiCommandline
 */
class StateCacheService
{
    /** @var Memcached */
    protected $memcached;

    public function __construct(Memcached $memcached)
    {
        $this->memcached = $memcached;
    }

    public function set(string $key, $status)
    {
        $this->memcached->set($key, $status, 60 * 5);
    }

    public function get(string $key)
    {
        $value = $this->memcached->get($key);
        if ($this->memcached->getResultCode() === 3) {
            throw new HeatingException('memcached not running');
        }

        if ($this->memcached->getResultCode() === Memcached::RES_NOTFOUND) {
            return null;
        }
        return $value;
    }
}
