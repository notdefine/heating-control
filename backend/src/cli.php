<?php

namespace HeatingControl;

/**
 * For running actions from the linux command line
 * Example : `php src/cli.php import`
 */

use HeatingControl\Container\Combine;
use HeatingControl\Controller\CliStoreController;
use HeatingControl\Controller\CliQueueWatcherController;
use HeatingControl\Controller\CliReportController;
use HeatingControl\Controller\RelayController;
use HeatingControl\Service\HeatingManagementService;
use Html2Text\Html2Text;
use League\CommonMark\CommonMarkConverter;
use Slim\App;
use Slim\Container;
use Slim\Http\Environment;
use Slim\Http\Request;
use Slim\Http\Response;

require __DIR__ . '/../vendor/autoload.php';

if (PHP_SAPI !== 'cli') {
    die('run on commandline');
}

$config = require __DIR__ . '/../config/slim.php';

$app = new App(['settings' => $config]);

$container = Combine::init($app->getContainer());

$container['environment'] = function (Container $c) {
    $argv = $GLOBALS['argv'];
    array_shift($argv);
    $pathInfo = implode('/', $argv);
    return Environment::mock(['REQUEST_URI' => '/' . $pathInfo]);
};

$app->map(
    ['GET'],
    '/log-sensors',
    CliStoreController::class . ':logSensors'
);

$app->map(
    ['GET'],
    '/show-sensors',
    CliStoreController::class . ':showSensors'
);

$app->map(
    ['GET'],
    '/graph',
    CliReportController::class . ':graph'
);

// Use the non-cli route, it do the same
$app->map(
    ['GET'],
    '/gpio',
    RelayController::class . ':index'
);

$app->map(
    ['GET'],
    '/watcher',
    CliQueueWatcherController::class . ':index'
);

$app->map(
    ['GET'],
    '/burner-control',
    HeatingManagementService::class . ':updateStatus'
);

$container['errorHandler'] = function (Container $c) {
    return function (Request $request, Response $response, $exception) use ($c) {
        return $c['response']->withStatus(500)
            ->withHeader('Content-Type', 'text/text')
            ->write('Error: ' . $exception->getMessage());
    };
};

$container['notFoundHandler'] = function (Container $c) {
    //this is wrong, i'm not with http
    return function (Request $request, Response $response) use ($c) {

        $converter = new CommonMarkConverter();

        $description = file_get_contents(__DIR__ . '/../../docs/cli-commands.md');

        if ($description === false) {
            throw new HeatingException('Could not find the cli command description');
        }

        $html = $converter->convertToHtml($description);
        $html2TextConverter = new Html2Text(
            $html,
            ['width' => 120]
        );

        return $c['response']
            ->withStatus(404)
            ->withHeader('Content-Type', 'text/plain')
            ->write(
                'COMMAND NOT FOUND' . PHP_EOL . $html2TextConverter->getText()
            );
    };
};

// Keine HTML Fehlermeldung auf der Console anzeigen
unset($app->getContainer()['phpErrorHandler']);

$app->run();
