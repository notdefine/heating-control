<?php

namespace HeatingControl\State;

use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\RelayService;

/**
 * Represents the temperature measurement of a sensor
 */
class RelayState extends SwitchState implements StoreQueueInterface, StoreRrdInterface
{
    use MessageQueueTrait;

    const SENSOR_PREFIX = 'homeassistant/binary_sensor/';

    const DIAGRAM_PUMP_ID = 9;
    const DIAGRAM_BURNER_ID = 10;

    const BURNER_KEY = 'burner';
    const PUMP_KEY = 'pump';

    public function publishQueueDiscovery(MessageQueueService $client): string
    {
        $deviceClass = '';

        if ($this->getName() == self::BURNER_KEY) {
            $deviceClass = 'heat';
        }

        if ($this->getName() == self::PUMP_KEY) {
            $deviceClass = 'motion';
        }

        $client->publish(
            $this->getQueueDiscoveryUrl(),
            json_encode([
                    'name' => $this->getName(),
                    'state_topic' => $this->getQueueStateUrl(),
                    'device_class' => $deviceClass
                ])
        );

        return $this->getQueueDiscoveryUrl();
    }

    /**
     * Dont direct return a boolean value. RRDTool need a int or string
     */
    public function getRrdValue(): int
    {
        return (parent::getValue() === RelayService::IO_VALUE_ON) ? 10 : 5;
    }
}
