<?php

namespace HeatingControl\State;

/**
 * Represents the humidity measurement of a sensor
 */
abstract class HumiditySensorState extends State
{
    const SENSOR_PREFIX = 'homeassistant/sensor/';

    const UNIT_OF_MEASUREMENT = '%';
    const DEVICE_CLASS = 'humidity';
}
