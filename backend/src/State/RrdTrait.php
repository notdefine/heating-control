<?php

namespace HeatingControl\State;

trait RrdTrait
{
    public function getRrdValue(): int
    {
        return parent::getValue();
    }
}
