<?php


namespace HeatingControl\State;

interface StoreRrdInterface
{
    public function getRrdValue(): int;
}
