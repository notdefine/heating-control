<?php

namespace HeatingControl\State;

use HeatingControl\Service\MessageQueueService;

trait MessageQueueTrait
{
    protected function getQueueDiscoveryUrl()
    {
        return static::SENSOR_PREFIX . $this->getId() . '/config';
    }

    public function getQueueStateUrl(): string
    {
        return static::SENSOR_PREFIX . $this->getId() . '/state';
    }

    /**
     * @param MessageQueueService $messageService
     * @return string
     */
    public function publishQueueValue(MessageQueueService $messageService): string
    {
        $messageService->publish(
            $this->getQueueStateUrl(),
            (string)$this->getQueueValue()
        );

        return $this->getQueueStateUrl();
    }

    public function getQueueValue(): string
    {
        return (parent::getValue() == 1) ? 'ON' : 'OFF';
    }
}
