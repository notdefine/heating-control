<?php
declare(strict_types=1);

namespace HeatingControl\State;

abstract class State
{
    /** @var string Im Regelfall eine Temperatur, könnte aber auch z.B. % Luftfeuchtigkeit sein */
    protected $value;

    /** @var string (optional from the Settings) Human readable sensor description */
    protected $name;

    /** @var string Sensor identification */
    protected $identification;

    /** @var integer (optional from the Settings) For Mapping to RRDTool */
    protected $id;

    /** @var bool All Sensors in Database are configured, all new sensors not */
    protected $configured = false;

    /**
     * @return array
     */
    public function extract(): array
    {
        return [
            'id' => $this->getId(),
            'value' => $this->getValue(),
            'identification' => $this->getIdentification(),
            'name' => $this->getName(),
            'configured' => $this->isConfigured()
        ];
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return State
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     */
    public function setValue(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getIdentification(): string
    {
        return $this->identification;
    }

    /**
     * @param string $identification
     * @return State
     */
    public function setIdentification(string $identification): State
    {
        $this->identification = $identification;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return State
     */
    public function setName(string $name): State
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return bool
     */
    public function isConfigured(): bool
    {
        return $this->configured;
    }

    /**
     * @param bool $configured
     */
    public function setConfigured(bool $configured)
    {
        $this->configured = $configured;
    }

    /**
     * @param array $data
     */
    public function hydrate(array $data)
    {
        $this->setId($data['id']);
        $this->setName($data['name']);
        $this->setIdentification($data['identification']);
        $this->setValue($data['value']);
        $this->setConfigured($data['configured']);
    }
}
