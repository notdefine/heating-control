<?php

namespace HeatingControl\State;

use HeatingControl\Service\MessageQueueService;

/**
 * Represents the temperature measurement of a sensor
 */
class TemperatureSensorState extends State implements StoreRrdInterface, StoreQueueInterface
{
    use MessageQueueTrait, RrdTrait;

    const SENSOR_PREFIX = 'homeassistant/sensor/';

    public function publishQueueDiscovery(MessageQueueService $client): string
    {
        $client->publish(
            $this->getQueueDiscoveryUrl(),
            json_encode(
                [
                    'name' => $this->getName(),
                    'device_class' => 'temperature',
                    'state_topic' => $this->getQueueStateUrl(),
                    'unit_of_measurement' => '°C'
                ]
            )
        );

        return $this->getQueueDiscoveryUrl();
    }

    public function getQueueValue(): string
    {
        return parent::getValue();
    }
}
