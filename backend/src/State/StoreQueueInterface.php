<?php


namespace HeatingControl\State;

use HeatingControl\Service\MessageQueueService;

interface StoreQueueInterface
{
    public function getQueueValue(): string;

    public function getQueueStateUrl(): string;

    public function publishQueueDiscovery(MessageQueueService $client);

    public function publishQueueValue(MessageQueueService $client): string;
}
