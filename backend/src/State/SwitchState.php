<?php

namespace HeatingControl\State;

use HeatingControl\Service\MessageQueueService;

/**
 * Represents the temperature measurement of a sensor
 */
class SwitchState extends State implements StoreQueueInterface
{
    use MessageQueueTrait;
    const SENSOR_PREFIX = 'homeassistant/switch/';

    const ECO_KEY = 'eco';

    public function publishQueueDiscovery(MessageQueueService $client): string
    {
        $client->publish(
            $this->getQueueDiscoveryUrl(),
            json_encode(
                [
                    'name' => $this->getName(),
                    'state_topic' => $this->getQueueStateUrl(),
                    'command_topic' => $this->getQueueStateUrl()
                ]
            )
        );

        return $this->getQueueDiscoveryUrl();
    }
}
