<?php

namespace HeatingControl;

error_reporting(E_ALL);

use HeatingControl\Container\Combine;
use HeatingControl\Controller\ApiDocumentationController;
use HeatingControl\Controller\BurnerController;
use HeatingControl\Controller\CliQueueWatcherController;
use HeatingControl\Controller\RelayController;
use HeatingControl\Controller\SensorSettingController;
use HeatingControl\Controller\SensorsValueController;
use HeatingControl\Controller\StatusController;
use HeatingControl\Middleware\CorsMiddleware;
use RKA\SessionMiddleware;
use Slim\App;
use Zeuxisoo\Whoops\Provider\Slim\WhoopsMiddleware;

require __DIR__ . '/../vendor/autoload.php';

$config = require __DIR__ . '/../config/slim.php';

$app = new App(['settings' => $config]);

Combine::init($app->getContainer());

if (PHP_SAPI !== 'cli' && PHP_SAPI !== 'phpdbg') {
    $app->add(new WhoopsMiddleware);

    $app->add(new SessionMiddleware([
        'name' => 'heating',
        'secure' => true
    ]));
}

$app->get(
    '/',
    ApiDocumentationController::class . ':index'
);

$app->get(
    '/status',
    StatusController::class . ':status'
);

$app->group('/api', function ($app) {
    $app->get(
        '/sensors',
        SensorsValueController::class . ':all'
    );

    $app->get(
        '/sensors/{identification}',
        SensorsValueController::class . ':identification'
    );

    $app->get(
        '/sensorByName/{name}',
        SensorsValueController::class . ':name'
    );

    $app->any(
        '/sensorSettings/{id}',
        SensorSettingController::class . ':id'
    );
    $app->map(
        ['GET'],
        '/watcher',
        CliQueueWatcherController::class . ':index'
    );

    $app->map(
        ['GET'],
        '/relay',
        RelayController::class . ':index'
    );

    $app->map(
        ['GET'],
        '/relay/{gpio}',
        RelayController::class . ':gpio'
    );

    $app->map(
        ['GET'],
        '/heatingStatus',
        BurnerController::class . ':index'
    );
})->add(CorsMiddleware::class);

return $app;
