<?php

namespace HeatingControl;

$config['displayErrorDetails'] = true;
$config['addContentLengthHeader'] = false;
$config['debug'] = true;

return $config;
