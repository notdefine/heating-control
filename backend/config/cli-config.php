<?php

// Needed for composer doctrine-init

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Console\ConsoleRunner;

/** @var Slim\App $app */
$app = require_once __DIR__ . '/../src/bootstrap.php';

// replace with mechanism to retrieve EntityManager in your app
$entityManager = $app->getContainer()->get(EntityManager::class);

return ConsoleRunner::createHelperSet($entityManager);
