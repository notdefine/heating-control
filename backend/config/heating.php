<?php
declare(strict_types=1);

namespace HeatingControl;

use HeatingControl\Service\RelayService;
use HeatingControl\State\RelayState;

return [
    'debug' => true,
    'diagrammWeekly' => [
        // Eine Liste der Sensoren die im Wochendiagramm nicht auftauchen sollen
        'skipSensors' => [
            'Leitung Warm',
            'Wärmetauscher'
        ]
    ],
    'diagrammColors' => [
        /* Use good distinguishable colors */
        '#000000',
        '#e6194b',
        '#3cb44b',
        '#ffe119',
        '#0082c8',
        '#f58231',
        '#911eb4',
        '#46f0f0',
        '#f032e6',
        '#d2f53c',
        '#fabebe'
    ],

    // Reduce water temperature at night for X degrees
    'nightlySubstract' => 10,
    'frostProtectionWaterTemperature' => 24,
    'missingMappingValueFallback' => 24,
    'threshold' => 2,

    'eco-mode-sink' => 5,

    'nightTimeStart' => 23,
    'nightTimeEnd' => 6,

// !!! Below this Line you MUST change the Settings to your environment
    'systemSensorsIds' => [
        /** Set this to your Sensor which is outside */
        'outside' => '10-00080282b5f8',
        'returnFlow' => '10-00080282b91c'
    ],

    RelayService::CONFIG_KEY => [
        /** Set this to your Sensor which is outside */
        RelayState::BURNER_KEY => 'gpio26',
        RelayState::PUMP_KEY => 'gpio20',
    ]
];
