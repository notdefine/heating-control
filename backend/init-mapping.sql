insert into desired_values (outside_temp, return_flow_temp)
VALUES (- 35, 36),
       (- 34, 36),
       (- 33, 36),
       (- 32, 36),
       (- 31, 36),
       (- 30, 36),
       (- 29, 36),
       (- 28, 36),
       (- 27, 36),
       (- 26, 36),
       (- 25, 36),
       (- 24, 36),
       (- 23, 36),
       (- 22, 36),
       (- 21, 36),
       (- 20, 36),
       (- 19, 36),
       (- 18, 36),
       (- 17, 36),
       (- 16, 36),
       (- 15, 36),
       (- 14, 36),
       (- 13, 36),
       (- 12, 36),
       (- 11, 36),
       (- 10, 36),
       (- 9, 36),
       (- 8, 36),
       (- 7, 36),
       (- 6, 36),
       (- 5, 36),
       (- 4, 36),
       (- 3, 35),
       (- 2, 34),
       (- 1, 32),
       (0, 32),
       (1, 32),
       (2, 32),
       (3, 32),
       (4, 32),
       (5, 31),
       (6, 30),
       (7, 30),
       (8, 29),
       (9, 29),
       (10, 28),
       (11, 28),
       (12, 27),
       (13, 27),
       (14, 26),
       (15, 26),
       (16, 25),
       (17, 25),
       (18, 23),
       (19, 23),
       (20, 23),
       (21, 11),
       (22, 11),
       (23, 11),
       (24, 1);
