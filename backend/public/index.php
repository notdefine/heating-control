<?php

declare(strict_types=1);

namespace HeatingControl;

require __DIR__ . '/../vendor/autoload.php';

// Show also errors which are not cached by Slim, like Mosquitto Connection errors
try {
    $app = require __DIR__ . '/../src/bootstrap.php';
    $app->run();
} catch (HeatingException $exception) {
    header('Content-Type: text/plain');
    echo $exception->getMessage() . PHP_EOL;
    echo 'File ' . $exception->getFile() . ' Line' . $exception->getLine();
    die;
}
