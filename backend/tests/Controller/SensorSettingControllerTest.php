<?php

namespace HeatingControl\Test\Controller;

use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use HeatingControl\Controller\SensorSettingController;
use HeatingControl\Entity\SensorSettingEntity;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Test\IntegrationTestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Slim\Http;

/*
1) Doctrine Mock nach SensorSettingServiceTest.php Klasse verschieben
2) Middleware Test aufbauen wie SensorsMiddlewareTestphp

*/

class SensorSettingControllerTest extends IntegrationTestCase
{

    /**
     * for the GET Method
     */
    public function testSensorSettingsWithMissingSensor()
    {
        // We need a request and response object to invoke the action
        $environment = Http\Environment::mock([
            'REQUEST_METHOD' => 'GET'
        ]);
        $request = Http\Request::createFromEnvironment($environment);

        $response = new Http\Response();
        $action = new SensorSettingController(new SensorSettingService($this->getEntityManger()));

        $response = $action->id($request, $response, ['id' => '28-000008fc6545']);


        $this->assertSame('Sensor : "28-000008fc6545" does not exists', (string)$response->getBody());
    }

    /**
     * @return EntityManager|object|ObjectProphecy
     * @throws OptimisticLockException
     */
    protected function getEntityManger()
    {
        $em = parent::getEntityManger();

        $prophet = $this->prophesize(ObjectRepository::class);
        $prophet->findBy(['identification' => '28-000008fc6545'])->willReturn(null);
        $entity = new SensorSettingEntity();
        $entity->setId(99);
        $entity->setIdentification('28-000008fc6546');
        $entity->setName('Drinnen');
        $prophet->findBy(['identification' => '28-000008fc6546'])->willReturn([0 => $entity]);
        $objectRepoMock = $prophet->reveal();

        $someRepoMock = 'TODO';

        $em->getRepository('MyBundle:SomeClass')->willReturn($someRepoMock);
        //mock with any parameter
        $em->getRepository(Argument::any())->willReturn($objectRepoMock);
        return $em->reveal();
    }

    // TBD Test anpassen darauf das doctrine den sensor finden würde

    /**
     * for the GET Method
     */
    public function testSensorSettingsWithExistingSensor()
    {
        $request = parent::getGetRequest();

        $response = new Http\Response();
        $action = new SensorSettingController(new SensorSettingService($this->getEntityManger()));

        $response = $action->id($request, $response, ['id' => '28-000008fc6546']);
        $this->assertSame(
            '{"id":99,"identification":"28-000008fc6546","name":"Drinnen"}',
            (string)$response->getBody()
        );
    }
}
