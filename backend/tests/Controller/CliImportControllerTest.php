<?php

namespace HeatingControl\Test\Controller;

use HeatingControl\Controller\CliStoreController;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\SensorStoreInterface;
use HeatingControl\State\TemperatureSensorState;
use HeatingControl\Test\IntegrationTestCase;
use Slim\Http;

class CliImportControllerTest extends IntegrationTestCase
{
    public function testLogSensors()
    {
        $request = parent::getGetRequest();
        $response = new Http\Response();

        $prophet = $this->prophesize(SensorStateService::class);
        $prophet->getAllSensorValues()->willReturn([
            (new TemperatureSensorState())
                ->setName('Testsensor')
        ]);

        /** @var SensorStateService $sensorService */
        $sensorService = $prophet->reveal();

        $prophet = $this->prophesize(SensorStoreInterface::class);
        $prophet->storeValues(
            (new TemperatureSensorState())
                ->setName('Testsensor')
        )->willReturn(true);

        /** @var SensorStoreInterface $sensorStore */
        $sensorStore = $prophet->reveal();

        $action = new CliStoreController(
            $sensorService,
            $sensorStore
        );

        $response = $action->logSensors($request, $response, []);
        $this->assertSame('"Values stored"', (string)$response->getBody());
    }
}
