<?php


namespace HeatingControl\Test\Controller;

use HeatingControl\Controller\RelayController;
use HeatingControl\Service\MessageQueueService;
use HeatingControl\Service\RelayService;
use HeatingControl\Test\IntegrationTestCase;
use Slim\Http;

class RelayControllerTest extends IntegrationTestCase
{
    public function testGetStatus()
    {
        $request = parent::getGetRequest();

        $response = new Http\Response();

        $prophet = $this->prophesize(RelayService::class);
        $prophet->getRelay('gpio21')->willReturn(1);
        $relayService = $prophet->reveal();

        $prophet = $this->prophesize(MessageQueueService::class);
        $messageService = $prophet->reveal();

        $action = new RelayController(
            $relayService,
            $messageService
        );

        $response = $action->gpio($request, $response, ['gpio' => 'gpio21']);
        $this->assertSame('Controler: gpio21 has status 1', (string)$response->getBody());
    }
}
