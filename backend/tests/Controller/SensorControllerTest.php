<?php

namespace HeatingControl\Test;

use HeatingControl\Service\SensorStateService;
use HeatingControl\State\TemperatureSensorState;
use Slim\Http;
use Slim\Route;

class SensorControllerTest extends IntegrationTestCase
{
    /**
     * for the GET Method
     */
    public function testSensorsGetAllSensors()
    {
        // We need a request and response object to invoke the action
        $environment = Http\Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI' => '/api/sensors'
        ]);

        $request = Http\Request::createFromEnvironment($environment);

        $route = new Route('', '', function () {
        });
        $request = $request->withAttribute('route', $route);

        // Only want to test the middleware, so mock the complete service

        /** @var SensorStateService| $sensorService */
        $sensorService = $this->prophesize(SensorStateService::class);

        $sensorValue1 = new TemperatureSensorState();
        $sensorValue1->setValue('10.123123123');
        $sensorValue1->setIdentification('10-00080282b5f8');
        $sensorValue1->setName('innen');
        $sensorValue1->setConfigured(true);

        $sensorValue2 = new TemperatureSensorState();
        $sensorValue2->setValue('2.22');
        $sensorValue2->setIdentification('10-00080282b5f9');
        $sensorValue2->setConfigured(false);

        $sensorService->getAllSensorValues()->willReturn([1 => $sensorValue1, 2 => $sensorValue2]);
        $sensorService = $sensorService->reveal();

        $slimApp = $this->getApp();

        // Hier will ich den DContainer modifizieren, so das der test direkt auf der App laufen kann
        $container = $slimApp->getContainer();

        $container[SensorStateService::class] = $sensorService;

        $response = $slimApp->process($request, new Http\Response());

        $this->assertSame(
            '{"1":{"id":null,"value":"10.123123123","identification":"10-00080282b5f8","name":"innen","configured":true},"2":{"id":null,"value":"2.22","identification":"10-00080282b5f9","name":null,"configured":false}}',
            (string)$response->getBody()
        );
    }

    /**
     * for the GET Method
     */
    public function testSensorsGetOneSensors()
    {
        // We need a request and response object to invoke the action
        $environment = Http\Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI' => '/api/sensors/20-00080282b5f8'
        ]);

        $request = Http\Request::createFromEnvironment($environment);

        $route = new Route('', '', function () {
        });
        $request = $request->withAttribute('route', $route);

        // Only want to test the middleware, so mock the complete service

        /** @var SensorStateService| $sensorService */
        $sensorService = $this->prophesize(SensorStateService::class);

        $sensorValue = new TemperatureSensorState();
        $sensorValue->setId(9);
        $sensorValue->setValue('10.123123123');
        $sensorValue->setIdentification('20-00080282b5f8');
        $sensorValue->setName('innen');
        $sensorValue->setConfigured(true);

        $sensorService->getSensorValueBySensorIdentification('20-00080282b5f8')->willReturn($sensorValue);
        $sensorService = $sensorService->reveal();

        $slimApp = $this->getApp();

        // Hier will ich den DContainer modifizieren, so das der test direkt auf der App laufen kann
        $container = $slimApp->getContainer();
        $container[SensorStateService::class] = $sensorService;

        $response = $slimApp->process($request, new Http\Response());

        $this->assertSame(
            '{"id":9,"value":"10.123123123","identification":"20-00080282b5f8","name":"innen","configured":true}',
            (string)$response->getBody()
        );
    }
}
