<?php

namespace HeatingControl\Test\Entity;

use HeatingControl\State\TemperatureSensorState;
use PHPUnit\Framework\TestCase;

class SensorsValueEntityTest extends TestCase
{
    public function testHydrator()
    {
        $sensorValue = new TemperatureSensorState();
        $sensorValue->setValue('10.123123123');
        $sensorValue->setIdentification('10-00080282b5f8');
        $sensorValue->setName('innen');
        $sensorValue->setId(2);

        $this->assertEquals([
            'id' => 2,
            'value' => '10.123123123',
            'identification' => '10-00080282b5f8',
            'name' => 'innen',
            'configured' => false
        ], $sensorValue->extract());

        $sensorValueHydrate = new TemperatureSensorState();
        $sensorValueHydrate->hydrate($sensorValue->extract());
        $this->assertEquals($sensorValue, $sensorValueHydrate);
    }
}
