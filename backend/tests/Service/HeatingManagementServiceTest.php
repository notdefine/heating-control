<?php
declare(strict_types=1);

namespace HeatingControl\Test\Controller;

use HeatingControl\Service\HeatingManagementService;
use HeatingControl\Service\NightSettingsService;
use HeatingControl\Service\RelayService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\StateCacheService;
use HeatingControl\Service\TemperatureMappingService;
use HeatingControl\State\RelayState;
use HeatingControl\State\TemperatureSensorState;
use HeatingControl\Test\IntegrationTestCase;
use Psr\Log\LoggerInterface;

class HeatingManagementServiceTest extends IntegrationTestCase
{
    public function testMustBurnerBeActive()
    {

        list(
            $sensorService,
            $relayService,
            $nightSettingService,
            $mappingService,
            $memcachedService,
            $logger
            ) = $this->getMocks();

        $bcs = new HeatingManagementService(
            ['threshold' => 1],
            $sensorService->reveal(),
            $relayService->reveal(),
            $nightSettingService->reveal(),
            $mappingService->reveal(),
            $memcachedService->reveal(),
            $logger->reveal()
        );

        $this->assertFalse($bcs->mustBurnerBeActive(20, 23));
        $this->assertFalse($bcs->mustBurnerBeActive(20, 22));
        $this->assertNull($bcs->mustBurnerBeActive(20, 21));
        $this->assertNull($bcs->mustBurnerBeActive(20, 20));
        $this->assertNull($bcs->mustBurnerBeActive(20, 19));
        $this->assertTrue($bcs->mustBurnerBeActive(20, 18));
        $this->assertTrue($bcs->mustBurnerBeActive(20, 17));

        $bcs = new HeatingManagementService(
            ['threshold' => 2],
            $sensorService->reveal(),
            $relayService->reveal(),
            $nightSettingService->reveal(),
            $mappingService->reveal(),
            $memcachedService->reveal(),
            $logger->reveal()
        );

        $this->assertFalse($bcs->mustBurnerBeActive(20, 23));
        $this->assertNull($bcs->mustBurnerBeActive(20, 22));
        $this->assertNull($bcs->mustBurnerBeActive(20, 21));
        $this->assertNull($bcs->mustBurnerBeActive(20, 20));
        $this->assertNull($bcs->mustBurnerBeActive(20, 19));
        $this->assertNull($bcs->mustBurnerBeActive(20, 18));
        $this->assertTrue($bcs->mustBurnerBeActive(20, 17));
    }

    protected function getMocks()
    {
        //$prophet->exec('cat /sys/class/gpio/gpio26/value')->willReturn(1);
        return [
            $sensorService = $this->prophesize(SensorStateService::class),
            $relayService = $this->prophesize(RelayService::class),
            $nightSettingService = $this->prophesize(NightSettingsService::class),
            $mappingService = $this->prophesize(TemperatureMappingService::class),
            $memcachedService = $this->prophesize(StateCacheService::class),
            $logger = $this->prophesize(LoggerInterface::class)
        ];
    }

    /**
     * @dataProvider expectedHeatingBehavior
     */
    public function testUpdateStatus($outsideTemp, $returnTemp, $destTemp, $threshold, $lastBurnerStatus, $burnerStatus, $pumpStatus)
    {
        list(
            $sensorService,
            /** @var RelayService $relayService */
            $relayService,
            $nightSettingService,
            $mappingService,
            $memcachedService,
            $logger
            ) = $this->getMocks();

        $outsideSensor = new TemperatureSensorState();
        $outsideSensor->setValue($outsideTemp);

        $sensorService->getSensorValueBySensorIdentification('10-xx1')->willReturn($outsideSensor);

        $returnTempSensor = new TemperatureSensorState();
        $returnTempSensor->setValue($returnTemp);

        $sensorService->getSensorValueBySensorIdentification('10-xx2')->willReturn($returnTempSensor);

        $mappingService->getReturnFlowValueForOutsideTemp($outsideTemp)->willReturn($destTemp);

        $memcachedService->get('10-xx1')->willReturn(null); // This will force the function updateStatus() to read the temperature value from the sensorServie
        $memcachedService->get('10-xx2')->willReturn(null);
        $memcachedService->get('burner')->willReturn($lastBurnerStatus);
        $memcachedService->get('pump')->willReturn(true);
        $memcachedService->set('burner', $burnerStatus)->shouldBeCalled();
        $memcachedService->set('pump', $pumpStatus)->shouldBeCalled();
        $memcachedService->get('eco')->willReturn(false);

        // This is Part of the testet behavior
        $relayService->setRelay('gpio01', $burnerStatus)->shouldBeCalled();
        $relayService->setRelay('gpio02', $pumpStatus)->shouldBeCalled();

        $bcs = new HeatingManagementService(
            [
                'frostProtectionWaterTemperature' => 10,
                'threshold' => $threshold,
                'systemSensorsIds' => [
                    'outside' => '10-xx1',
                    'returnFlow' => '10-xx2'
                ],
                RelayService::CONFIG_KEY => [
                    RelayState::BURNER_KEY => 'gpio01',
                    RelayState::PUMP_KEY => 'gpio02'
                ]
            ],
            $sensorService->reveal(),
            $relayService->reveal(),
            $nightSettingService->reveal(),
            $mappingService->reveal(),
            $memcachedService->reveal(),
            $logger->reveal()
        );

        $bcs->updateStatus();
    }

    public function expectedHeatingBehavior(): array
    {
        return [
            [$outsideTemp = '10', $returnTemp = '26', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '25', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '24', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '23', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '22', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '21', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '20', $destTemp = 25, $threshold = 0, $lastBurnerStatus = false, $burner = true, $pump = true],

            [$outsideTemp = '10', $returnTemp = '26', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '25', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '24', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '23', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '22', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '21', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '20', $destTemp = 25, $threshold = 0, $lastBurnerStatus = true, $burner = true, $pump = true],

            // treshold = 2
            [$outsideTemp = '10', $returnTemp = '26', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '25', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '24', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '23', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '22', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '21', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '20', $destTemp = 25, $threshold = 2, $lastBurnerStatus = false, $burner = true, $pump = true],

            [$outsideTemp = '10', $returnTemp = '28', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = false, $pump = true],
            [$outsideTemp = '10', $returnTemp = '27', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '26', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true], // because the burner was on, keep it on even in the threshold scope
            [$outsideTemp = '10', $returnTemp = '24', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '23', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '22', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '21', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
            [$outsideTemp = '10', $returnTemp = '20', $destTemp = 25, $threshold = 2, $lastBurnerStatus = true, $burner = true, $pump = true],
        ];
    }
}
