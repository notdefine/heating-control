<?php

namespace HeatingControl\Test\Service;

use HeatingControl\Entity\SensorSettingEntity;
use HeatingControl\Service\ShellCommandService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Service\StateCacheService;
use HeatingControl\State\TemperatureSensorState;
use PHPUnit\Framework\TestCase;

final class SensorServiceTest extends TestCase
{
    const EXAMPLE_SENSOR_ID = '10.123123123';

    public function testGetSensorValueBySensorIdentification()
    {
        $cmdService = $this->prophesize(ShellCommandService::class);
        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . escapeshellarg(self::EXAMPLE_SENSOR_ID) . SensorStateService::FILTER)
            ->willReturn("27500");

        $sensorSettingService = $this->prophesize(SensorSettingService::class);

        $sensorSettingService->getSettingByIdentification(self::EXAMPLE_SENSOR_ID)->willReturn(
            $this->getSensorSettingEntity()
        );

        /** @var SensorStateService $sensorService */
        $sensorService = new SensorStateService(
            $cmdService->reveal(),
            $sensorSettingService->reveal(),
            $this->prophesize(StateCacheService::class)->reveal(),
            []
        );

        $sensorValue = $sensorService->getSensorValueBySensorIdentification(self::EXAMPLE_SENSOR_ID);
        $this->assertInstanceOf(
            TemperatureSensorState::class,
            $sensorValue
        );

        $this->assertEquals(
            self::EXAMPLE_SENSOR_ID,
            $sensorValue->getIdentification()
        );

        $this->assertEquals(
            27.5,
            $sensorValue->getValue()
        );
    }

    protected function getSensorSettingEntity()
    {
        $entity = new SensorSettingEntity();
        $entity->setIdentification(self::EXAMPLE_SENSOR_ID);
        $entity->setName('UnitTestSensor');
        $entity->setId(2);
        return $entity;
    }

    public function testGetSensorValueBySensorName()
    {
        $cmdService = $this->prophesize(ShellCommandService::class);
        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . escapeshellarg(self::EXAMPLE_SENSOR_ID) . SensorStateService::FILTER)
            ->willReturn("13500");

        /** @var SensorSettingService $sensorSettingService */
        $sensorSettingService = $this->prophesize(SensorSettingService::class);

        $sensorSettingService->getSettingByName("sensor1")->willReturn(
            $this->getSensorSettingEntity()
        );

        /** @var SensorStateService $sensorService */
        $sensorService = new SensorStateService(
            $cmdService->reveal(),
            $sensorSettingService->reveal(),
            $this->prophesize(StateCacheService::class)->reveal(),
            []
        );

        $sensorValue = $sensorService->getSensorValueBySensorName('sensor1');
        $this->assertInstanceOf(
            TemperatureSensorState::class,
            $sensorValue
        );

        $this->assertEquals(
            self::EXAMPLE_SENSOR_ID,
            $sensorValue->getIdentification()
        );

        $this->assertEquals(
            13.5,
            $sensorValue->getValue()
        );
    }

    /**
     * Important is to test, if the service difference between configured and unconfigured Sensors
     * by recognises if there is a setting in the database
     *
     * - /sys/bus/w1/devices/w1_bus_master1/ returns three Sensors
     * - Database has two configurations
     *
     */
    public function testGetAllSensorValues()
    {
        $cmdService = $this->prophesize(ShellCommandService::class);
        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . escapeshellarg('10-00080282b5f8') . SensorStateService::FILTER)
            ->willReturn("11500");
        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . escapeshellarg('10-00080282b91c') . SensorStateService::FILTER)
            ->willReturn("12500");
        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . escapeshellarg('10-00080282bb0e') . SensorStateService::FILTER)
            ->willReturn("13500");

        $cmdService->exec('cat ' . SensorStateService::SYS_DIR . '[12][0-9]-*/name')
            ->willReturn("10-00080282b5f8\n10-00080282b91c\n10-00080282bb0e");


        /** @var SensorSettingService $sensorSettingService */
        $sensorSettingService = $this->prophesize(SensorSettingService::class);


        $sensorSettingService->getSettingByIdentification("10-00080282b5f8")->willReturn(
            $this->getSensorSettingEntity()
        );

        $sensorSettingService->getSettingByIdentification("10-00080282b91c")->willReturn(
            false
        );

        $sensorSettingService->getSettingByIdentification("10-00080282bb0e")->willReturn(
            $this->getSensorSettingEntity()
        );

        /** @var SensorStateService $sensorService */
        $sensorService = new SensorStateService(
            $cmdService->reveal(),
            $sensorSettingService->reveal(),
            $this->prophesize(StateCacheService::class)->reveal(),
            ['relayIds' => ['burner' => '1', 'pump' => '2']]
        );
        $result = $sensorService->getAllSensorValues();

        $this->assertTrue($result['10-00080282b5f8']->isConfigured());
        $this->assertFalse($result['10-00080282b91c']->isConfigured());
        $this->assertTrue($result['10-00080282bb0e']->isConfigured());
        $this->assertEquals(13.5, $result['10-00080282bb0e']->getValue());
    }
}
