<?php

namespace HeatingControl\Test\Controller;

use DateTime;
use HeatingControl\Service\NightSettingsService;
use PHPUnit\Framework\TestCase;

class NightSettingsServiceTest extends TestCase
{
    public function testGetCurrentHour()
    {
        $service = new NightSettingsService([
            'nightTimeStart' => 23,
            'nightTimeEnd' => 6,
        ], new DateTime('2019-11-01 00:00:00'));
        $this->assertEquals(0, $service->getCurrentHour());
        $service = new NightSettingsService([
            'nightTimeStart' => 23,
            'nightTimeEnd' => 6,
        ], new DateTime('2019-11-01 12:03:59'));
        $this->assertEquals(12, $service->getCurrentHour());
        $service = new NightSettingsService([
            'nightTimeStart' => 23,
            'nightTimeEnd' => 6,
        ], new DateTime('2019-11-21 23:59:59'));
        $this->assertEquals(23, $service->getCurrentHour());
    }

    /**
     * @dataProvider provider
     */
    public function testIsNightlySinkActive($expected, DateTime $date)
    {
        $service = new NightSettingsService([
            'nightTimeStart' => 23,
            'nightTimeEnd' => 6,
        ], $date);
        $this->assertEquals($expected, $service->isNightlySinkActive());
    }

    public function provider()
    {
        return [
            [true, new DateTime('2019-11-01 00:00:00')],
            [true, new DateTime('2019-11-01 03:02:00')],
            [false, new DateTime('2019-11-01 09:05:00')],
            [false, new DateTime('2019-11-01 12:00:00')],
            [false, new DateTime('2019-11-01 19:00:00')],
            [false, new DateTime('2019-11-01 22:59:00')],
            [true, new DateTime('2019-11-01 23:00:00')],
            [true, new DateTime('2019-11-01 23:00:01')],
        ];
    }
}
