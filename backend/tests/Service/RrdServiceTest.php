<?php


namespace HeatingControl\Test\Service;

use DateTime;
use DateTimeImmutable;
use HeatingControl\Service\RrdService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\ShellCommandService;
use HeatingControl\State\TemperatureSensorState;
use HeatingControl\Test\IntegrationTestCase;

class RrdServiceTest extends IntegrationTestCase
{
    /**
     * @expectedException \HeatingControl\HeatingException
     */
    public function testGetImageCommandWhithMissingValues()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        $commandService = $prophet->reveal();

        $prophet = $this->prophesize(SensorStateService::class);
        $sensorService = $prophet->reveal();

        $service = new RrdService($commandService, $sensorService, []);
        $this->assertEquals('#', $service->getImageCommand(new DateTime()));
    }

    public function testGetImageCommand()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        #    $prophet->findBy(['name' => 'test'])->willReturn(null);
        $commandService = $prophet->reveal();

        $prophet = $this->prophesize(SensorStateService::class);
        $sensorService = $prophet->reveal();

        $service = new RrdService($commandService, $sensorService, [
            'diagrammColors' => [
                '#000000',
                '#e6194b',
                '#3cb44b',
                '#ffe119',
            ]
        ]);
        $this->assertEquals(
            <<<EOL
rrdtool graph /home/pi/heating-control/backend/public/images/temp.svg -a SVG --title="Temperatur 36h 20.04.2019 17:15" --start -129600 --vertical-label "Grad Celsius"  'DEF:probe1=/home/pi/.temperature/temp.rrd:sensor1:AVERAGE' 'LINE1:probe1#e6194b:Außen' 'DEF:probe3=/home/pi/.temperature/temp.rrd:sensor3:AVERAGE' 'LINE1:probe3#ffe119:Wohnzimmer' 'GPRINT:probe3:LAST:Tempsensor Last Temp\: %2.1lf' --font WATERMARK:0.1:DejaVuSansMono --font UNIT:0:DejaVuSansMono --font AXIS:6:DejaVuSansMono --font LEGEND:8:. -w 1000 -h350 --full-size-mode --x-grid HOUR:1:HOUR:2:HOUR:2:86400:%H:00 -E
EOL,
            $service->getImageCommand(
                new DateTimeImmutable('2019-04-20 17:15'),
                (new TemperatureSensorState())
                    ->setId(1)
                    ->setName('Außen'),
                (new TemperatureSensorState())
                    ->setId(3)
                    ->setName('Wohnzimmer')
            )
        );
    }

    public function testGetImageCommandWeekly()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        $commandService = $prophet->reveal();

        $prophet = $this->prophesize(SensorStateService::class);
        $sensorService = $prophet->reveal();

        $service = new RrdService($commandService, $sensorService, [
            'diagrammWeekly' => [
                // Eine Liste der Sensoren die im Wochendiagramm nicht auftauchen sollen
                'skipSensors' => [
                    'Skipmich',
                ]
            ],
            'diagrammColors' => [
                '#000000',
                '#e6194b',
                '#3cb44b',
                '#ffe119',
            ]
        ]);
        $this->assertEquals(
            <<<EOL
rrdtool graph /home/pi/heating-control/backend/public/images/temp-week.svg -a SVG --title="Temperatur 8 Tage 20.04.2019 17:15" --start -691200 --vertical-label "Grad Celsius"  'DEF:probe1=/home/pi/.temperature/temp.rrd:sensor1:AVERAGE' 'LINE1:probe1#e6194b:Außen' 'DEF:probe3=/home/pi/.temperature/temp.rrd:sensor3:AVERAGE' 'LINE1:probe3#ffe119:Wohnzimmer' --font WATERMARK:0.1:DejaVuSansMono --font UNIT:0:DejaVuSansMono --font AXIS:6:DejaVuSansMono --font LEGEND:8:. -w 1000 -h350 --full-size-mode -E
EOL,
            $service->getImageCommandWeekly(
                new DateTimeImmutable('2019-04-20 17:15'),
                (new TemperatureSensorState())
                    ->setId(1)
                    ->setName('Außen'),
                (new TemperatureSensorState())
                    ->setId(3)
                    ->setName('Wohnzimmer'),
                (new TemperatureSensorState())
                    ->setId(2)
                    ->setName('Skipmich')
            )
        );
    }
}
