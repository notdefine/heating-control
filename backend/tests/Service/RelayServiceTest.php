<?php
declare(strict_types=1);

namespace HeatingControl\Test\Controller;

use HeatingControl\Service\RelayService;
use HeatingControl\Service\ShellCommandService;
use HeatingControl\Test\IntegrationTestCase;

class RelayServiceTest extends IntegrationTestCase
{

    public function testGetRelais()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        $prophet->exec('cat /sys/class/gpio/gpio26/value')->willReturn(1);
        $commandServiceMock = $prophet->reveal();

        $relayService = new RelayService($commandServiceMock);
        $this->assertEquals(1, $relayService->getRelay('gpio26'));
    }

    public function testsetRelais()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        $prophet->exec("echo '1' > /sys/class/gpio/gpio21/value")->willReturn("");
        $commandServiceMock = $prophet->reveal();

        $relayService = new RelayService($commandServiceMock);
        $this->assertTrue($relayService->setRelay('gpio21', RelayService::IO_VALUE_ON));
    }

    /**
     * @expectedException \TypeError
     * @expectedExceptionMessage must be of the type bool
     */
    public function testsetRelaisWithInvalidStatus()
    {
        $prophet = $this->prophesize(ShellCommandService::class);
        $commandServiceMock = $prophet->reveal();

        $relayService = new RelayService($commandServiceMock);
        $this->assertFalse($relayService->setRelay('gpio21', 3));
    }
}
