<?php


namespace HeatingControl\Test;

use HeatingControl\Service\ShellCommandService;
use HeatingControl\Service\SensorStateService;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Service\SensorStoreRrd;
use PHPUnit\Framework\TestCase;

class SensorStoreRrdTest extends TestCase
{
    public function testCreateUpdate()
    {
        /** @var ShellCommandService $commandService */
        $commandService = $this->prophesize(ShellCommandService::class);
        $commandService = $commandService->reveal();

        /** @var ShellCommandService $commandService */
        $sensorSettingService = $this->prophesize(SensorSettingService::class);
        $sensorSettingService = $sensorSettingService->reveal();

        $rrd = new SensorStoreRrd($commandService, new SensorStateService($commandService, $sensorSettingService));

        $sensor1 = new TemperatureSensorEntity();

        $sensor1->setName('test1');
        $sensor1->setValue('123.3');
        $sensor1->setIdentification('ident1');
        $sensor1->setId(2);

        $sensor2 = new TemperatureSensorEntity();

        $sensor2->setName('test2');
        $sensor2->setValue('65');
        $sensor2->setIdentification('10-0008034a6e53');
        $sensor2->setId(1);

        $sensor3 = new TemperatureSensorEntity();

        $sensor3->setName('Unconfigured Sensor');
        $sensor3->setValue('33');
        $sensor3->setIdentification('28-000008fc6545');
        $sensor3->setId(10);

        $cmd = $rrd->getUpdateCommand([
            $sensor1,
            $sensor2,
            $sensor3
        ]);
        $this->assertEquals(
            'rrdtool update /home/pi/.temperature/temp.rrd N:65:123.3:U:U:U:U:U:U:U:33',
            $cmd
        );
    }
}
