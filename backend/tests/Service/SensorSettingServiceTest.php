<?php

namespace HeatingControl\Test\Service;

use Doctrine\Common\Persistence\ObjectRepository;
use HeatingControl\Entity\SensorSettingEntity;
use HeatingControl\Service\SensorSettingService;
use HeatingControl\Test\IntegrationTestCase;

final class SensorSettingServiceTest extends IntegrationTestCase
{
    const EXAMPLE_SENSOR_ID = '10.123123123';

    public function testGetSettingByNameSettingMissing()
    {
        $em = $this->getEntityManger();

        $prophet = $this->prophesize(ObjectRepository::class);
        $prophet->findBy(['name' => 'test'])->willReturn(null);
        $objectRepoMock = $prophet->reveal();


        $em->getRepository(SensorSettingEntity::class)->willReturn($objectRepoMock);
        //mock with any parameter
        $service = new SensorSettingService($em->reveal());
        $setting = $service->getSettingByName('test');
        $this->assertFalse($setting);
    }

    public function testGetSettingByNameSettingExists()
    {
        $em = $this->getEntityManger();

        $prophet = $this->prophesize(ObjectRepository::class);
        $prophet->findBy(['name' => 'test'])->willReturn([0 => new SensorSettingEntity()]);
        $objectRepoMock = $prophet->reveal();

        $em->getRepository(SensorSettingEntity::class)->willReturn($objectRepoMock);
        //mock with any parameter
        $service = new SensorSettingService($em->reveal());
        $setting = $service->getSettingByName('test');

        $this->assertInstanceOf(SensorSettingEntity::class, $setting);
    }

    public function testGetSettingByIdentification()
    {
        $em = $this->getEntityManger();

        $prophet = $this->prophesize(ObjectRepository::class);
        $prophet->findBy(['identification' => 'test'])->willReturn([0 => new SensorSettingEntity()]);
        $objectRepoMock = $prophet->reveal();

        $em->getRepository(SensorSettingEntity::class)->willReturn($objectRepoMock);
        //mock with any parameter
        $service = new SensorSettingService($em->reveal());
        $setting = $service->getSettingByIdentification('test');

        $this->assertInstanceOf(SensorSettingEntity::class, $setting);
    }

    protected function getSensorSettingEntity()
    {
        $entity = new SensorSettingEntity();
        $entity->setIdentification(self::EXAMPLE_SENSOR_ID);
        $entity->setName('UnitTestSensor');
        return $entity;
    }
}
