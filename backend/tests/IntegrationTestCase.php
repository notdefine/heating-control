<?php

namespace HeatingControl\Test;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Prophecy\ProphecyInterface;
use Slim\App;
use Slim\Container;
use Slim\Http\Environment;
use Slim\Http\Request;

abstract class IntegrationTestCase extends TestCase
{

    public function getGetRequest(): Request
    {
        // We need a request and response object to invoke the action
        $environment = Environment::mock([
            'REQUEST_METHOD' => 'GET',
        ]);
        return Request::createFromEnvironment($environment);
    }

    /**
     * @return App
     * @throws OptimisticLockException
     */
    protected function getApp(): App
    {
        /** @var App $slim */
        $slim = require __DIR__ . '/../src/bootstrap.php';

        $container = $slim->getContainer();

        $container['entityManager'] = function (Container $c) {
            return $this->getEntityManger()->reveal();
        };

        // No html tags in phpunit error messages
        unset($slim->getContainer()['phpErrorHandler']);

        return $slim;
    }

    /**
     * @return EntityManager|ObjectProphecy
     * @throws OptimisticLockException
     * @throws ORMException
     */
    protected function getEntityManger()
    {
        /**
         * @var ProphecyInterface|EntityManager $prophet
         */
        $prophet = $this->prophesize(EntityManager::class);
        $prophet->persist(Argument::any())->willReturn(null);
        $prophet->flush(Argument::any())->willReturn(null);

        return $prophet;
    }
}
