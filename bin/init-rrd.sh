#!/usr/bin/env bash

echo Create an empty RRD Database for sensor diagramms? This is only used once.
echo CTRL+C to Abort

read

mkdir -p /home/pi/.temperature

echo Working ...

rrdtool create /home/pi/.temperature/temp.rrd --step 60 \
DS:sensor1:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor2:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor3:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor4:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor5:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor6:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor7:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor8:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor9:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600 \
DS:sensor10:GAUGE:300:-30:90 RRA:AVERAGE:0.5:2:525600
