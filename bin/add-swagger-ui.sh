#!/usr/bin/env bash

set -x
LATEST_RELEASE=$(curl -L -s -H 'Accept: application/json' https://github.com/swagger-api/swagger-ui/releases/latest)
LATEST_VERSION=$(echo ${LATEST_RELEASE} | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
curl -sSL -o swagger-ui.zip https://github.com/swagger-api/swagger-ui/archive/${LATEST_VERSION}.zip
mkdir -p backend/public/api-docs/
rm -rf public/docs/api-docs/*
unzip -d swagger-ui swagger-ui.zip
cp -R swagger-ui/*/dist/* backend/public/api-docs/
rm -rf swagger-ui
rm swagger-ui.zip
sed -i -e "s#\"http://petstore.swagger.io/v2/swagger.json\"#location.origin + \"/swagger.yml\"#g" backend/public/api-docs/index.html
