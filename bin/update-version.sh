#!/usr/bin/env bash
START_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export PATH=~/.config/composer/vendor/bin:$PATH

cd $START_DIR/../backend
git pull || exit
composer up

cd $START_DIR/../frontend
npm install
ng build