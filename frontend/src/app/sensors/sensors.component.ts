import {Component, OnInit} from '@angular/core';
import {SensorsService} from '../sensors.service';
import {ConfigService} from '../config.service';
import {differentColors} from '../sensor-colors';
import {SensorValue} from '../sensor-value';
import {NGXLogger} from "ngx-logger";

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.scss'],
})
export class SensorsComponent implements OnInit {

  differentColors;
  sensors: SensorValue[];

  ngOnInit() {
    this._sensorService.sensorObserver.subscribe(res => this.sensors = res);
  }

  view: any[] = [400, 300];

  constructor(private _sensorService: SensorsService, private config: ConfigService, private logger: NGXLogger) {
    this.differentColors = differentColors;
  }

  onSelect(event) {
    this.logger.debug(event);
  }
}
