import {Component, OnInit} from '@angular/core';
import {ConfigService} from '../config.service';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: [NGXLogger]
})
export class HomeComponent implements OnInit {

  static defaultSwitchInterval = 10000;

  switchInterval: number;
  noWrapSlides: boolean = false;

  images = [
    'temp.svg',
    'temp-week.svg',
  ];

  imagesUrls = [];

  constructor(private config: ConfigService, private logger: NGXLogger) {
    this.switchInterval = HomeComponent.defaultSwitchInterval;
    this.logger.info('Home called');
  }

  ngOnInit() {
    this.logger.debug('Use URL for backend: ' + this.config.backendApi);
    this.refreshImages();

    IntervalObservable.create(180 * 1000).subscribe(x => {
      this.refreshImages();
    });
  }

  toggleSwitch() {
    if (this.switchInterval > 0) {
      this.switchInterval = 0;
    } else {
      this.switchInterval = HomeComponent.defaultSwitchInterval;
    }
  }

  public refreshImages() {
    this.imagesUrls = [];
    this.images.forEach(function (image) {
      this.imagesUrls.push(this.config.backendApi + '/images/' + image + '?' +
        new Date().getTime());
    }, this);
    this.logger.debug('Load new graph from ' + this.imagesUrls);
  }
}
