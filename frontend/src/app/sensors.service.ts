import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {BehaviorSubject, Observable} from 'rxjs';
import {ConfigService} from './config.service';
import {IntervalObservable} from 'rxjs/observable/IntervalObservable';
import {SensorValue} from './sensor-value';
import {NGXLogger} from "ngx-logger";

@Injectable()
export class SensorsService {

    /*
    One of the variants of Subjects is the BehaviorSubject, which has a notion of
    "the current value". It stores the latest value emitted to its consumers, and
    whenever a new Observer subscribes, it will immediately receive the "current
    value" from the Behavior new SensorValue(Subject).
     */
    private sensors = new BehaviorSubject<any>([]);

    sensorObserver = this.sensors.asObservable();

    constructor(private http: HttpClient, private config: ConfigService, private logger: NGXLogger) {

        this.readSensors();

        IntervalObservable.create(60 * 1000).subscribe(x => {
            this.readSensors();
        });

    }

    public getJSON(): Observable<any> {
        return this.http.get(this.config.backendApi + '/api/sensors');
    }

    public postJSON(sensor: SensorValue): Observable<any> {

        let url = this.config.backendApi +
            '/api/sensorSettings/' +
            sensor.identification;

        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'my-auth-token',
            }),
        };
        return this.http.post(url, sensor, httpOptions);
    }

    public setSensor(sensor: SensorValue) {
        this.logger.info(SensorValue);

        this.postJSON(sensor).subscribe(data => {
            this.logger.info(data);
        });
    }

    protected readSensors() {
        this.getJSON().subscribe(data => {

            var dataRound = [];

            data = Object.values(data);

            // Temperature precision to 1/10 grad celsius
            for (let sensor of data) {
                sensor.value = Number.parseFloat(sensor.value).toFixed(1);
                let sensorValue = new SensorValue(sensor);
                dataRound.push(sensorValue);
            }

            this.logger.info(dataRound);

            this.sensors.next(dataRound);
        });
    }
}
