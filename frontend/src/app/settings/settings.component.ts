import {Component, OnInit, TemplateRef} from '@angular/core';
import {SensorsService} from '../sensors.service';
import {ConfigService} from '../config.service';
import {BsModalService} from 'ngx-bootstrap/modal';
import {BsModalRef} from 'ngx-bootstrap';
import {differentColors} from '../sensor-colors';
import {SensorValue} from '../sensor-value';
import {SettingDialogComponent} from '../setting-dialog/setting-dialog.component';
import {NGXLogger} from "ngx-logger";

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  differentColors;
  sensors: SensorValue[];

  selectedSensor: SensorValue;

  constructor(
      private _sensorService: SensorsService, private config: ConfigService,
      private modalService: BsModalService, private logger: NGXLogger
  ) {
    this.differentColors = differentColors;
  }

  ngOnInit() {
    this._sensorService.sensorObserver.subscribe(res => this.sensors = res);
  }

  openModalWithComponent(sensor: SensorValue) {
    const initialState = {
      sensor: sensor,
      title: 'Edit ' + sensor.identification,
    };
    this.modalService.show(
        SettingDialogComponent,
        {initialState}
    );
  }
}
