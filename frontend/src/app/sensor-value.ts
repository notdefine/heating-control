export interface SensorValue {
  id?: string,
  value?: number,
  identification?: string,
  name?: string,
  configured?: boolean
}

export class SensorValue implements SensorValue {
  constructor(array) {
    this.id = array.id;
    this.configured = array.configured;
    this.identification = array.identification;
    this.name = array.name;
    this.value = array.value;
  }
}