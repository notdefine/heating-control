import {Injectable} from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {NGXLogger} from "ngx-logger";

@Injectable()
export class ConfigService {

  /* Wenn kein Backend aktiv ist (nicht via url parameter api übergeben wurde, dummy Daten verwenden. */
  public backendApi = 'assets';

  constructor(private ActiveRoute: ActivatedRoute, private logger: NGXLogger) {
  }

  getParams() {
    this.ActiveRoute.queryParams.subscribe((params: Params) => {
      let _queryParem = params;
      if (_queryParem.hasOwnProperty('api')) {
        this.backendApi = _queryParem.api;
      }
      this.logger.debug(_queryParem);
    });
  }
}
