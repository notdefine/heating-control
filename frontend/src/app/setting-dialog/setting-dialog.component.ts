import {Component, OnInit} from '@angular/core';
import {BsModalService} from 'ngx-bootstrap/modal';
import {SensorValue} from '../sensor-value';
import {SensorsService} from '../sensors.service';
import {NGXLogger} from 'ngx-logger';

@Component({
  selector: 'app-setting-dialog',
  templateUrl: './setting-dialog.component.html',
  styleUrls: ['./setting-dialog.component.scss'],
})
export class SettingDialogComponent implements OnInit {

  title: string;
  sensor: SensorValue;

  constructor(
    public modalService: BsModalService, public sensorService: SensorsService, private logger: NGXLogger) {
  }

  ngOnInit() {
  }

  closeModal() {
    this.logger.info(this.sensor);
    this.sensorService.setSensor(this.sensor);
    this.modalService.hide(1);
  }

}
