import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeComponent} from './home/home.component';
import {SettingsComponent} from './settings/settings.component';
import {AlertModule} from 'ngx-bootstrap/alert';
import {CarouselModule} from 'ngx-bootstrap/carousel';
import {MenuComponent} from './menu/menu.component';
import {FormsModule} from '@angular/forms';
import {ConfigService} from './config.service';
import {SensorsComponent} from './sensors/sensors.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {SensorsService} from './sensors.service';
import {HttpClientModule} from '@angular/common/http';
import {BsModalService, ModalModule} from 'ngx-bootstrap/modal';
import {SettingDialogComponent} from './setting-dialog/setting-dialog.component';
import {BsDropdownModule, CollapseModule} from 'ngx-bootstrap';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {NgxNavbarModule} from 'ngx-bootstrap-navbar';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SettingsComponent,
    MenuComponent,
    SensorsComponent,
    SettingDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AlertModule.forRoot(),
    CarouselModule.forRoot(),
    CollapseModule.forRoot(),
    BsDropdownModule.forRoot(),
    FormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    HttpClientModule,
    ModalModule.forRoot(),
    LoggerModule.forRoot({
      serverLoggingUrl: '/api/logs',
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.ERROR
    }),
    NgxNavbarModule
  ],
  providers: [ConfigService, SensorsService, BsModalService],
  bootstrap: [AppComponent],
  entryComponents: [
    SettingDialogComponent,
  ],
})
export class AppModule {
}
