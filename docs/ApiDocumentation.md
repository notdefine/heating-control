# List of ressources

## Common

 - [Last generated temperature graph](images/temp.svg)
 - [openhab2 GUI](https://heating.fritz.box:8080)
 - [Home Assistant](https://heating.fritz.box:8123)


## API

 - [Heating status](status)
 - [swagger.yml api documentation](swagger.yml)
 - [List of all connected sensors](api/sensors) 
 - [Status of relais](api/relay)
 - [Night Time reduction active](api/nighttimeReduction)
 - [Burner/Heating overall status](api/heatingStatus)

