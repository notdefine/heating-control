# Setup iRobot Roomba Vacuum

[Roomba integraion](https://www.home-assistant.io/integrations/roomba/)

## Getting Blid and Password

[Unofficial iRobot Roomba 980 python library (SDK)](https://github.com/NickWaterton/Roomba980-Python#how-to-get-your-usernameblid-and-password)

.local/bin/roomba-getpassword 

## CV or numpy module not found, falling back to PIL

`sudo apt install python3-ope/etc/ssl/openssl.cnfncv`

## The headers or library files could not be found for jpeg, a required dependency when compiling Pillow from source.

`sudo apt-get install libjpeg-dev zlib1g-dev`

## SSL: DH_KEY_TOO_SMALL

Lösche folgende Zeile aus der /etc/ssl/openssl.cnf

CipherString = DEFAULT@SECLEVEL=2
