# Setup

## home-assistant Software

[Read Setup](https://www.home-assistant.io/hassio/installation/)

Setupverlauf bei mir (bitte den Link oben als Referenz nehmen)

```bash
sudo -i
apt-get install software-properties-common -y
apt-get update
apt-get install -y apparmor-utils apt-transport-https avahi-daemon ca-certificates curl dbus jq network-manager socat
curl -fsSL get.docker.com | sh

curl -sL "https://raw.githubusercontent.com/home-assistant/hassio-installer/master/hassio_install.sh" | bash -s -- -m raspberrypi3
systemctl restart hassio-supervisor.service
```


## Add Mqqt Message Queue to home-assistant

Damit hass die Sensoren uns Schalter automatisch erkennt, muss hass mit der Queue verbunden
werden wo die Heizungsdaten landen, dazu wird die `configure.yaml` angepasst.

mcedit /usr/share/hassio/homeassistant/configuration.yaml

```yaml
mqtt: 
  broker: localhost 
  discovery: true 
  discovery_prefix: homeassistant
```

**localhost** ist in diesem Fall die Queue die beim einrichten auf dem Raspberry installiert wurde.

## hide single sensors in UI

https://www.home-assistant.io/docs/configuration/customizing-devices/

# Example complete config
/usr/share/hassio/homeassistant/configuration.yaml

```yaml
# Configure a default setup of Home Assistant (frontend, api, etc)
default_config:

# Uncomment this if you are using SSL/TLS, running in Docker container, etc.
# http:
#   base_url: example.duckdns.org:8123

# Text to speech
tts:
  - platform: google_translate

group: !include groups.yaml
automation: !include automations.yaml
script: !include scripts.yaml

device_tracker:
  - platform: fritz
    host: 192.168.178.1

vacuum:
  - platform: roomba
    host: 192.168.178.35
    username: 69B8860C82323530
    password: ":1:1573226762:Cx3IotVdiWHrHuIt"

mqtt:
  broker: localhost
  discovery: true
  discovery_prefix: homeassistant

media_player:
  - platform: kodi
    host: libreelec

homeassistant:
  customize: !include customize.yaml
```

## customize.yaml

```yaml
device_tracker.retropie:
  hidden: true
device_tracker.heating:
  hidden: true
device_tracker.libreelec:
  hidden: true
device_tracker.samsung_galaxy_s7:
  hidden: true
device_tracker.roomba_69b8860c82323530:
  hidden: true
device_tracker.android_d3a57407a411bde9:
  hidden: true
device_tracker.fritz_repeater:
  hidden: true
device_tracker.notebook:
  hidden: true
device_tracker.fritz_box:
  hidden: true
device_tracker.t_eimers_pc:
  hidden: true
binary_sensor.updater:
  hidden: true
device_tracker.pc_9635_aff_fec7_e831:
  hidden: true
device_tracker.samsung_galaxy_s7_2:
  hidden: true
```

## automations.yaml
```yaml
- id: '1573603852664'
  alias: Kellerlicht automatisch aus (in der Nacht)
  description: nach 23-07 Uhr
  trigger:
  - device_id: 7ab800ce05574d24a9bd1332f7072e60
    domain: light
    entity_id: light.kellerflur
    for:
      hours: 0
      minutes: 1
      seconds: 0
    platform: device
    type: turned_on
  condition:
  - after: '21:00'
    before: 07:00
    condition: time
  action:
  - alias: ''
    data:
      entity_id: light.kellerflur
    service: light.turn_off
```

groups.yaml
```yaml
allpersons:
  name: Jemand zu Hause
  entities:
    - person.petra
    - person.thomas
```

```yaml
title: Berkenberg
views:
  - path: default_view
    title: Home
    badges:
      - entity: binary_sensor.burner
      - entity: binary_sensor.pump
      - entity: person.petra
      - entity: person.thomas
      - entity: sensor.aussen
      - entity: sun.sun
    cards:
      - entity: weather.berkenberg
        type: weather-forecast
      - entities:
          - switch.eco_mode
        title: Schalter
        type: entities
      - entities:
          - vacuum.roomba
        title: Staubsauger
        type: entities
      - entity: sensor.aussen
        graph: line
        name: Außen
        theme: default
        type: sensor
  - path: heizung
    title: Heizung
    badges:
      - entity: binary_sensor.burner
      - entity: binary_sensor.pump
      - entity: sensor.aussen
      - entity: sensor.fussboden_vorlauf
      - entity: sensor.heizkorper
      - entity: sensor.heizschlange_boiler
      - entity: sensor.warmwasser_boiler
      - entity: sensor.warmwasser_gebrauch
      - entity: sun.sun
    cards:
      - entity: sensor.aussen
        graph: line
        name: Außen
        theme: default
        type: sensor
    panel: false
  - path: temperatur
    title: Temperaturverlauf
    cards:
      - entities:
          - entity: sensor.aussen
          - entity: sensor.warmwasser_gebrauch
          - entity: sensor.warmwasser_boiler
          - entity: sensor.heizkorper
          - entity: sensor.fussboden_vorlauf
          - entity: sensor.aussen
        hours_to_show: 24
        refresh_interval: 0
        title: Temperaturverlauf
        type: history-graph
    panel: true
  - path: switches
    title: schalter
    cards:
      - type: light
        entity: light.kellerflur
        name: Kellerflur
```
