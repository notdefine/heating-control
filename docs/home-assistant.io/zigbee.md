
# conbee II Setup

Der USB Stick wird automatisch erkannt /dev/ttyACM0

## deconz

Ist ein Tool über das man über eine Weboberfläche die Zigbee Geräte konfigurieren
kann. Diese kann man _danach_ in hass verwenden.

deconz muss nicht auf dem raspi installiert werden, dass macht man über hass.io Oberfläche als Add-on.
Die Sensoren werden dann in der deconz Oberfläche konfiguriert (In Add-ons gibts den Link zur Oberfläche).

```                       
 Hass.io -> System -> Host system -> Hardware page.
 
 Enable in integrations
  Link with deCONZ
 
 Unlock your deCONZ gateway to register with Home Assistant.
 
     Go to deCONZ Settings -> Gateway -> Advanced
     Press "Authenticate app" button
 
```

## Verwendete Hardware

- ConBee II das universelle Zigbee USB-Gateway (Arbeitet stabil) 
- Xiaomi Aqara Fenster Tür Sensor (im Test, Anbindung funktionierte einwandfrei)
- iluminize Zigbee 3.0 Schalt-Aktor Mini (Arbeitet stabil)


## Eurotronic Spirit Zigbee

Muss von Hand eingerichtet werden. Dazu wird sich über den VNC Zugang zum Ad-On verbunden vnc://heating.fritz.box:5900 ,
das muss aber zuvor im Ad-On VNC aktiviert werden.

[Setup](./setup-Eurotronic%20Spirit.png)

https://forum.iobroker.net/topic/28785/how-to-eurotronic-spirit-zigbee-mit-conbee-ii/2

so you’ve successfully paired them and you can see the device as part of your mesh in VNC?
if not done already: select the right most circle on the spirit device card displayed when using VNC, select “basic” in the pop-out and press “read” on the tab called “cluster info” (to the left of the screen) afterwards. things like “manufacturer name” should be populated by doing that.
when this is done there should be enough info about the device to make Home Assistant discover it as a climate. you may have to restart Home Assistant Core to finish the process.
