# Remote Zugriff auf Raspberry ohne dyndns und Portfreigabe

## Stichwörter

Dazu richtet man den Raspi so ein, das er selbständig einen Port zu einem öffentlichen Server offen hält.
Von diesem öffenlichen Server kann man dann auf den Raspi zugreifen ohne die IP zu kennen.

 - reverse tunnel
 - ssh
 - autossh

```
apt-get autossh
# privater key vom zielserveruser
chmod 600 vserver-pi.key
# Verbindung testen mit
sudo su -
autossh -N -R10000:localhost:22 -i /home/pi/vserver-pi.key -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" pi@play-mobilos.de
```

## Auf raspberry Pi

in /etc/rc.local ablegen

```bash
autossh -N -R10000:localhost:22 -i /home/pi/vserver-pi.key -o "ServerAliveInterval 10" -o "ServerAliveCountMax 3" pi@play-mobilos.de
```

## Auf zentralem (öffentlich erreichbarem) Server

Kann man dann via ssh auf den Raspi zugreifen

```bash
ssh pi@localhost -p10000
```
