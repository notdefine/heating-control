# Service Structure

```plantuml
@startuml
title Controler/Service Structure
left to right direction
skinparam monochrome true
skinparam componentStyle uml2

rectangle "Access" as access {
    [CLI cli.php (shell)] as shellCli
    [API Backend REST] as backend
}

rectangle "CLI Controller" as controllerCli{
    [CliReportController]
    [CliStoreController]
}

rectangle "API Controller" as controllerApi{
    [ApiDocumentationController]
    [BurnerController]
    [RelayController]
    [SensorSettingController]
    [SensorValueController]
    [StatusController]
}

rectangle "Services" as services {
    [SensorStateService]
    [StateCacheService]
    [RelayService]
    [HeatingManagementService]
    [RrdService]
    [SensorSettingService]
    [ShellCommandService]
    [MessageQueueService]
    [NightSettingsService]
    [TemperatureMappingService]
}

shellCli -> controllerCli
backend --> controllerApi
services <.. controllerCli
services <.. controllerApi

@enduml
```
