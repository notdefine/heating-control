```plantuml
@startuml
skinparam monochrome true
skinparam componentStyle uml2

title Kontextdiagramm Heating-Control


rectangle "Heating-Control Project" {
    [<b>PHP Backed API\nAPI Port 88] as be
    [<b>Angular Frontend\nUI Port 80] as fe
    (Cron) as cron

    cron -> be
    fe <-> be: Sensor Configuration
}

[CliMqttWatcher] as watcher
[Diagramm Images\nrrdtool] as image

[RelaisService] as relaysService
[SensorService] as sensorService

be --> relaysService
image --> fe

rectangle "Sensor Stores" as stores {
    [Store RRD] as rrd
    [Store Logger] as logger
    [Store Memcached] as memcached
    [Store mqtt - Mosquitto\nMessage Queue] as message_queue
}

rrd --> image

rectangle "Explorer 700 Card" as 700 {
    [1-Wire Bus] as one_wire
}

rectangle "Openhab2" {    
    [<b>Home Assitant\nOpenHab2\nUI Port 8080] as oh
    
    [switch on/off] as binding
    
    oh -> binding: rules
}
    

rectangle "Relais Card" as relays {
    [Switch 1] as relais1
    [Switch 2] as relais2
    [Switch 3] as relais3
}

oh <-- message_queue: read

be <-- sensorService : Autodetect Sensors\nRead Temperatures\n(once per min)

relaysService --> relays

watcher --> relaysService
binding --> message_queue: write
watcher <-up- message_queue: read

[Pumpe Fußboden] as pump
[Brenner] as burner
[DS1920\nAußen] as sensor1
[DS1920\nRücklauf] as sensor2

sensorService --> one_wire
sensorService -up-> stores

one_wire -- sensor1
sensor1 -- sensor2
sensor2 -- sensorX

relais1 -- pump
relais2 -- burner

@enduml
```
