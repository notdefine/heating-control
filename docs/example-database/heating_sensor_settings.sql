INSERT INTO heating.sensor_settings (id, identification, name) VALUES (1, '28-000008fc6545', 'Warmwasser Gebrauch');
INSERT INTO heating.sensor_settings (id, identification, name) VALUES (2, '10-0008034ae606', 'Warmwasser Boiler');
INSERT INTO heating.sensor_settings (id, identification, name) VALUES (3, '10-00080282b5f8', 'Außen');
INSERT INTO heating.sensor_settings (id, identification, name) VALUES (4, '10-0008034a6e53', 'Heizschlange Boiler');
INSERT INTO heating.sensor_settings (id, identification, name) VALUES (5, '10-00080282bb0e', 'Heizkörper');
INSERT INTO heating.sensor_settings (id, identification, name) VALUES (6, '10-00080282b91c', 'Fußboden Vorlauf');