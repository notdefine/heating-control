## Available CLI commands

```bash
php backend/cli.php log-sensors
```

 - show-sensors : Show current sensor values
 - log-sensors : Read and import current sensor values
 - graph : Generate diagramm of temperatures
 - gpio : Get Switch Status of the relais card
 - watcher : Look at the mqqt Message Queue for 'eco'-Mode from Openhab/Home-assistant (Persons out-of-home)
 - burner-control : Lock at the current Temperatures and turn pump/burner on/off. Call this function every minute
