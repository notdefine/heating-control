# Heating control (Fußboden-Heizungssteuerung)

[Heating Control Blog](https://www.notdefine.de/projects.php?project=heizungssteuerung-mit-raspberry-pi)

![Applicatiuon Preview](https://www.notdefine.de/projects/heizungssteuerung-mit-raspberry-pi/sensor-list.jpeg "Show all configured sensors")
![Applicatiuon Preview](https://www.notdefine.de/projects/heizungssteuerung-mit-raspberry-pi/temp.svg "Show temperature graph")

![System Structure](https://notdefine.de/projects/heizungssteuerung-mit-raspberry-pi/umweltdiagramm.png "System structure")
![Service Structure](https://notdefine.de/projects/heizungssteuerung-mit-raspberry-pi/ServiceStructure.png "Service structure")


## Sytem  Requirements

- PHP 7.3 for the Application
- xdebug 2.5 for Code Coverage
- npm 3.5.2 for using swagger-codegen
- node 10

## System Setup on a Raspberry PI

[Setup of the Raspberry operating system](SETUP.md)

## Run Application

### Setup Database for backend

It is needed to setup the database on the system first. (see [Setup.md](SETUP.md))

```bash
cd backend
composer doctrine-init
```

### Run backend application for local testing
```bash
cd backend
composer up
composer run serve
```

[Access the local running backend http://localhost:8081](http://localhost:8081) 
[Access the backend on Raspberry PI http://heating:88](http://heating:88) 


### Run frontend application for local testing 

If you call the frontend without the 'api' get parameter for the backend API it would show some dummy data.

```bash
cd frontend
npm install
./node_modules/@angular/cli/bin/ng serve
```

[Access the local running frontend http://localhost:4200](http://localhost:4200) 

### Combine

http://localhost:4200/?api=http:%2F%2localhost:8081

## Local development

### Test Backend Application

```bash
cd backend
php composer.phar run lint
php composer.phar run tests
php composer.phar run phpcs (phpcs-fix for auto correction errors)
```

### Generate static API documentation via swagger

```bash
cd backend
php composer.phar add-swagger-uiswagger
```

### Edit Swagger configuration file

```bash
docker run -p 80:8080 swaggerapi/swagger-editor
```

[Access the Editor on http://localhost:80](http://localhost:80)

### Get Logs

```bash
tail -f /var/log/syslog
```

## API

- http://heating.fritz.box:88/api/sensors
- http://heating.fritz.box:88/api/sensors/10-00080282b5f8
- http://heating.fritz.box:88/api/sensorByName/Au%C3%9Fen
- http://heating.fritz.box:88/api/sensorSettings/10-00080282b5f8
- http://heating.fritz.box:88/api/relay

// Get Relay controller Status
- http://heating.fritz.box:88/api/control/1
watcher

## Documentation

- [Contribute](CONTRIBUTING.md)
- [Umweltdiagramm](docs/umweltdiagramm.md)
- [ServiceStructure](docs/ServiceStructure.md)
