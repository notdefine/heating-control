

## Code Style 

- PSR-1, PSR-2
- Line Endings LF

See `composer run phpcs`
Use `git config core.hooksPath .githooks` to check Code before Commit


## Tests

Use prophesize in tests not mockBuilder

## API Structure

### Entity

Represents a single API entity which must not be the same as the database entity (API Entities 
which are generated from the backend like special selects like "favoriteArticles" 
which are joined Data are exposed also as API entities).

- /[entityName]/{$id} 
    Examples: users, cars, articles
    - PUT: Set a complete entity
      - Response: 201 on success, 400 on Error
    - PATCH: incremental update
    - DELETE: Delete selected entity
    - POST : -

### Entity collections

Access to a list of entities.

- /[entityName]/
    - POST: Create one or many entities
    - PUT: Create one or many entities. All existing entities are deleted
    - GET: TBD Accepts Pagination and Filter
    - DELETE: Drop all entities

### RPC/Command

### Pagination and Filter

### Documentation

Swagger YAML File

### HTTP Status Code

- 2xx positiv response
- 4xx negativ response
- 5xx should not happen (like Database Error)