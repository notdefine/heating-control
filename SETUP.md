# Software installieren

## Setup Raspberry Basis Betriebssystem

Die Steuerung basiert auf [Raspian Stretch Lite](https://www.raspberrypi.org/downloads/raspbian/), dieses wird als Image zur Verfügung gestellt.

## Booten vom USB Stick

Falls ihr eine Festplatte verwenden wollt (empfehlung!) dann müsst ihr zuerst einen Raspberry auf 
[USB Boot umstellen](https://www.raspberrypi.org/documentation/hardware/raspberrypi/bootmodes/msd.md)
Wenn ihr eine SD Karte verwenden wollt ist das nicht nötig.

### Linux "Raspian" auf Buster Lite Basis installieren

```bash
cat /proc/partitions # Hier den devicenamen für den USB Stick/ die USB Festplatte ablesen
wget "https://downloads.raspberrypi.org/raspbian_lite_latest"
sudo -i
# Bei mir aktuell die Version vom 2019-09-26
# Das Verzeichnis /dev/mmcblk0 durch euren devicenamen ersetzen
unzip -p raspbian_lite_latest | dd bs=1M status=progress of=/dev/mmcblk0
```

Nach dem abschluß des Befehls (dauert einige Minuten) die Speicherkarte einmal entfernen
und wieder in den PC stecken (noch nicht in den Raspberry!).

### SSH aktivieren und hostnamen vor dem ersten Bootvorgang anpassen

#### SSH

Quelle: https://www.raspberrypi.org/documentation/remote-access/ssh/

"For headless setup, SSH can be enabled by placing a file named ssh, without any extension, onto the boot partition of the SD card from another computer. When the Pi boots, it looks for the ssh file. If it is found, SSH is enabled and the file is deleted. The content of the file does not matter; it could contain text, or nothing at all."

Damit Raspbian SSH aktiviert nimmt man die SD Karte, nachdem man das Image auf die SD- Karte geschrieben hat, aus dem Rechner, steckt diese wieder ein 
und erstellt in der /boot-Partition des Raspbian-Systems eine leere Datei mit dem Namen ssh. 

```bash
cd /media/t-eimers/boot
sudo touch ssh
```

#### Hostname

Den Hostnamen auf `heating` ändern

```bash
cd /media/t-eimers/rootfs/etc
sudo mcedit hostname 
```


#### Raspberry mit neuer SD Karte bestücken und sich via SSH verbinden

Nun könnt ihr euch nach dem start des Raspberry via SSH verbinden. (Benutzer pi, Passwort raspberry).

```bash
ssh pi@heating
```

### Wifi/Wlan einrichten

Ich betreibe mein Raspberry dirket am Lan und muss daher nichts einrichten da dieser sich via DHCP konfiguriert.
Falls ihr WLAN benutz könnt ihr diese wie folgt einrichten:

```bash
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```

```text
network={
    ssid="hotspot"
    psk="..."
}
```

## Basis Softwarepakte installieren

Debian Stretch hat PHP 7.3 nativ dabei. Daher ist die Installation recht einfach.

### Stretch Pakete
```bash
sudo -i
apt-get update && apt-get dist-upgrade -y

apt-get install -y php7.3-cli php7.3-xml php7.3-mbstring php-xdebug php7.3-mysql mariadb-server \
    mariadb-client php-mysql mc atop htop secure-delete php-apcu libapache2-mod-php7.3 git \
    php7.3-mbstring php7.3-xml mariadb-server mariadb-client php7.3-mysql rrdtool php-curl \
    php7.3-zip php7.3-memcached memcached

apt autoremove -y
apt-get clean
```

### Quellcode auschecken (installieren)

Nicht als root!

```bash
cd /home/pi
git clone https://gitlab.com/notdefine/heating-control.git
# works only if you have properly set up your environment with the necessary SSH keys and whatnot, but in return you will 
# get a repository that permits you to commit/push the changes back to the remote repo.
# git clone git@gitlab.com:notdefine/heating-control.git
```

### RRD Digramm initalisieren

```bash
~/heating-control/bin/init-rrd.sh
```
 aufrufen

### Setup Node/npm

```bash
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.35.1/install.sh | bash
```

relogin to the system!

```bash
nvm install node #v13.1.0
```

### Apache einrichten

Einmal den Apache konfigurieren und neustarten und den Raspi aktualisieren.

#### /etc/apache2/sites-enabled/000-default.conf anpassen

```bash
sudo mcedit /etc/apache2/sites-enabled/000-default.conf
```

```text
<VirtualHost *:88>
	# PHP Backend

	#ServerName www.example.com

	ServerAdmin webmaster@localhost
	DocumentRoot /home/pi/heating-control/backend/public

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>

<VirtualHost *:80>
	# Compiled Frontend

	#ServerName www.example.com

	ServerAdmin webmaster@localhost
	DocumentRoot /home/pi/heating-control/frontend/dist

	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined

</VirtualHost>
```

#### /etc/apache2/ports.conf anpassen

```bash
sudo mcedit /etc/apache2/ports.conf
```

```text
# If you just change the port or add more ports here, you will likely also
# have to change the VirtualHost statement in
# /etc/apache2/sites-enabled/000-default.conf

Listen 80
Listen 88

<IfModule ssl_module>
	Listen 443
</IfModule>

<IfModule mod_gnutls.c>
	Listen 443
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
```

#### .htaccess overwrite aktivieren

Hiermit stellen wir ein, dass .htaccess Dateien in Unterordnern auch Optionen setzen können. Folgenden Teil hinzufügen

```bash
sudo mcedit /etc/apache2/apache2.conf
```

An das Ende anfügen:

```text
<Directory /home/pi/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
```

#### Apache Restart
```bash
sudo a2enmod expires
sudo a2enmod headers
sudo a2enmod rewrite
sudo systemctl restart apache2
sudo service apache2 restart
```

Jetzt sollte unter http://heating.fritz.box:88/ eine leere Seite erscheienen,
aber im error_log sollte stehen das er PHP Dateien nicht gefunden hat.

cat /var/log/apache2/error.log 

```text
PHP Fatal error:  require(): Failed opening required '/home/pi/heating-control/backend/public/../vendor/autoload.php' (include_path='.:/usr/share/php') in /home/pi/heating-control/backend/public/index.php on line 7
```

Wenn diese Ausgabe nicht kommt sollte man nicht weitermachen sondern nochmal das Setup des Apache Webservers
prüfen.


### Composer lokal installieren

Nicht als Root ausführen!

```bash
cd ~
# Get composer from https://getcomposer.org
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"

# Install composer global and remove local installation
php composer.phar global require composer/composer
export PATH=~/.config/composer/vendor/bin:$PATH
rm composer.phar

echo "export PATH=~/.config/composer/vendor/bin:$PATH" >> ~/.bashrc
```

Nach Aufruf von `composer --version` sollte etwas ähnliches wie `CComposer 1.9.1 2019-11-01 17:20:17` erscheinen. 

### Mysql (mariadb) einrichten

```bash
sudo mysql_secure_installation
```

- Das Root passwort ist leer
- Set root password? [Y/n] y
- Neues Passwort raspberrypi
- Remove anonymous users? [Y/n] y
- Disallow root login remotely? [Y/n] y
- Remove test database and access to it? [Y/n] y
- Reload privilege tables now? [Y/n]  y

Das root Passwort sollte man sich merken (z.B. raspberrypi) ;)

```bash
sudo mysql -u root -p
```

```mariadb
CREATE DATABASE heating;
CREATE USER 'heating'@'localhost' IDENTIFIED BY 'testing';
GRANT ALL PRIVILEGES ON heating.* TO 'heating'@'localhost';
quit
```
### Datbenbank für Zugriff von außen freigeben (nur nötig für Entwicklung)

if external access is needed. Then also set bind-adress to 0.0.0.0 in /etc/mysql/mariadb.conf.d/50-server.cnf

```bash
sudo mysql -u root -p
```

```mariadb
CREATE USER 'heating'@'%' IDENTIFIED BY 'testing';
GRANT ALL PRIVILEGES ON heating.* TO 'heating'@'%';
quit
```

[IDE Url](jdbc:mariadb://heating.fritz.box:3306/heating)

### mqtt Anbindung an OpenHab2/Home Automatisation mittels mosquitto (nicht optional, auch wenn openhab nicht verwendet wird)

```bash
sudo apt-get install -y php7.3-dev php-pear libmosquitto1 libmosquitto-dev mosquitto
#mosquitto-clients for debugging
sudo pecl channel-update pecl.php.net
sudo pecl install Mosquitto-alpha # Enter for autodetect
```
Then add `extension=mosquitto.so` to both php.ini. (after [PHP])

```bash
sudo mcedit /etc/php/7.3/cli/php.ini 
sudo mcedit /etc/php/7.3/apache2/php.ini 

sudo systemctl enable mosquitto
sudo systemctl start mosquitto
sudo systemctl status mosquitto  
```

Die Ausgabe vom letzten Befehl muss `Active: active (running)` beinhalten.

Beispiel wie man einen Sensor in der Queue kommuniziert.

```bash
mosquitto_pub -h 127.0.0.1 -p 1883 -t "homeassistant/sensor/sensor1/config" -m '{"name": "Sensortest", "device_class": "temperature"}'
```

### Heizungssteuerung einrichten

- Benötigte PHP Pakte installieren
- Datenbankstruktur einrichten
- Frontend bauen

```bash
cd ~/heating-control/backend/
composer up
composer doctrine-init

cd ~/heating-control/frontend/

npm install -g -p @angular/cli # 9.0.1
    
#npm install node-gyp@latest
#npm install node-sass
#npm install typescript@">=3.1.1 <3.3"

npm install

# Nach folgendem Befehl liegt in ~/heating-control/frontend/dist das kompilierte Frontend
ng build --prod
# Wenn das nicht erfolgreich funktiniert ggf. folgenden Befehl probieren (Raspi hat wenig Arbeitsspeicher)
node --max_old_space_size=512 node_modules/@angular/cli/bin/ng build --prod
```

Unter http://heating.fritz.box erscheint nun die Oberfläche der Steuerung (statische Ansicht mit Beispieldaten/-diagrammen)

### System aufrufen

http://heating.fritz.box?api=http://heating.fritz.box:88

## Sensoren einrichten

### 1-Wire Bus Kernelmodule automatisch laden

Dazu in der Datei `/etc/rc.local` folgende zwei Zeilen vor dem "exit 0" hinzufügen:

```bash
modprobe w1-gpio
modprobe w1-therm
``` 

### GPIO-Pullup aktivieren 

Folgendes am Ende der Datei `/boot/config.txt` hinzufügen.
```text
dtoverlay=w1-gpio-pullup
```

Und den Ton abschalten
```text
dtparam=audio=off
```
Einmal neu starten.

Jetzt sollte  `cat /sys/bus/w1/devices/w1_bus_master1/28-*/w1_slave | grep t=`

etwas ähnliches wie `13 02 4b 46 7f ff 0d 10 e7 t=33187` ausgeben.

```bash
cd heating-control/
php backend/src/cli.php show-sensors
```

sollte die Sensoren anzeigen die die Heizung erkannt hat

```text
array:6 [
  "10-00080282b5f8" => HeatingControl\State\TemperaturSensorState {#102
    #identification: "10-00080282b5f8"
    #value: 7.06
    #name: "Sensor 10"
    #id: 10
    #configured: false
  }
  ...
```

## Relaikarte vorbereiten

Dazu in der Datei `/etc/rc.local` folgende Code vor dem "exit 0" hinzufügen:

```bash
echo 26 > /sys/class/gpio/export
echo 20 > /sys/class/gpio/export
echo 21 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio26/direction
echo out > /sys/class/gpio/gpio20/direction
echo out > /sys/class/gpio/gpio21/direction

# Nach dem booten erstmal alles einschalten (so wie die relais auch eingestellt sind, wenn die spannung weg ist)
echo 1 > /sys/class/gpio/gpio26/value
echo 1 > /sys/class/gpio/gpio20/value
echo 1 > /sys/class/gpio/gpio21/value
```

```bash
sudo adduser www-data gpio
sudo systemctl restart apache2
```

Einmal neu starten.


## Auf Eco-Modus von Home Assistent reagieren

mit in rc.local

```bash
/home/pi/heating-control/bin/watcher.sh&
```

## Setup Cron

Um die Cronjobs der Steuerung einzurichten, müssen folgende beiden Zeilen an die `/etc/crontab` angehangen werden.
Wichtig ist, dass die Datei mit einer leeren Zeile endet. Nach der Anpassung muss der Cron einmal neu gestartet werden
via `sudo /etc/init.d/cron restart`

```text
* * * * * pi cd /home/pi/heating-control/backend && php src/cli.php log-sensors
* * * * * pi cd /home/pi/heating-control/backend && php src/cli.php burner-control
*/5 * * * * pi cd /home/pi/heating-control/backend && php src/cli.php graph
#0 1 * * * root rm /var/log/openhab2/*.log
```

## Zeitzone setzen

```bash
sudo dpkg-reconfigure tzdata
sudo dpkg-reconfigure locales
```

Wähle Europe/Berlin und [*] de_DE.UTF-8 UTF-8


## Watchdog installieren
119
Restart Raspberry if it hangs.

http://blog.ricardoarturocabral.com/2013/01/auto-reboot-hung-raspberry-pi-using-on.html

```bash
sudo apt-get install watchdog
echo "bcm2835_wdt" | sudo tee -a /etc/modules
sudo nano /etc/watchdog.conf
sudo systemctl enable watchdog.service
sudo systemctl start watchdog.service
```

`sudo mcedit /etc/watchdog.conf`

```text
watchdog-device = /dev/watchdog
min-memory=1
retry-timeout           = 60
repair-maximum          = 1
```

## Reduce writes on SD-Card (optional, erhöht die SD Lebenszeit)

Using iotop -bktoqqq I figured out most frequent write access. It turns out that also /var/cache/samba/ is frequently written to. So this also has to go to RAM in addition to /var/tmp/ where the new log files will be.

SD Karten sind nicht für dauernde Schreibzyklen ausgelegt, daher die Schreibvorgänge minimieren.

`sudo mcedit /etc/systemd/journald.conf`

```text
[Journal]
Storage=volatile
```

## Disable bluetooth
https://scribles.net/disabling-bluetooth-on-raspberry-pi/

sudo nano /boot/config.txt

Add below, save and close the file.
`dtoverlay=pi3-disable-bt`

```bash
sudo systemctl disable hciuart.service
sudo systemctl disable bluealsa.service
sudo systemctl disable bluetooth.service

sudo apt-get purge bluez -y
sudo apt-get autoremove -y
sudo systemctl mask serial-getty@ttyAMA0.service
```

## zsh Shell installieren (optional)

Ich bevorzuge die bash kompatible zsh Shell

```bash
sudo apt-get install zsh -y
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
```

und .zshrc befüllen mit
```bash
export ZSH="/home/pi/.oh-my-zsh"
ZSH_THEME="agnoster"
plugins=(
  git
  sudo
  z
)
source $ZSH/oh-my-zsh.sh
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# folgedene zeile aus der .bashrc übernehmen
export PATH=~/.config/composer/vendor/bin:/home/pi/.nvm/versions/node/v13.1.0/bin:/usr/local/bin:/usr/bin:/bin:/usr/games
```

Shell auf zsh umstellen, wenn nicht direkt bei der installation gemacht
```bash
chsh -s /bin/zsh
```

## Home Automatisation oder openhab2 installieren (optional)

Ich bevorzuge Home Assistant, schneller eingerichtet.

[Home Assistantt Setup Doku](./docs/home-assistant.io/SETUP.md)
[OpenHab Setup Doku](./docs/openhab2-settings/SETUP.md)

## reverse tunnel setup (optional)

[Setup autossh](docs/autossh.md)

## Oberfläche Aufrufen

### Frontend 

[Fronted NG App](http://heating.fritz.box)

### Backend

[Backend PHP App](http://heating.fritz.box:88)

### Frontend & Backend

[Gemeinsam ergeben diese die Heizungssteuerung](http://heating.fritz.box/?api=http:%2F%2Fheating.fritz.box:88)


# motes ( to be removed)

#notes

ng serve --host=0.0.0.0 --disable-host-check
http://localhost:4200/?api=http:%2F%2Fheating.fritz.box

sudo apt-get install wiringpi python-imaging
sudo apt-get install -y git-core python-dev
sudo apt-get install python-pip
sudo pip install spidev

## Image der SD Karte erstellen und Wiederherstellen:

### Backup erstellen
`sudo dd bs=1M if=/dev/sdd of=heating_control_2018_10_14.img`

### Backup wieder herstellen
`sudo dd bs=1M if=heating_control_2018_10_14.img.img of=/dev/sdd`

# Quellen

https://sven-s.de/mosquitto-mqtt-on-raspberry-pi-fails-on-startup/
http://www.marcsblog.de/2016/01/mit-openhab-werte-speichern-und-in-charts-darstellen-am-beispiel-der-zwischensteckdose-mit-leistungsmessung/    
