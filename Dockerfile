FROM ubuntu:eoan
# Container to run Tests in Gitlab-CI, see https://gitlab.com/notdefine/heating-control/container_registry
# docker pull registry.gitlab.com/notdefine/heating-control:ci

MAINTAINER notdefine@gmx.de
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y zip php7.3-dev php7.3-cli php7.3-curl php-mbstring php-pear php7.3-memcached php7.3-phpdbg \
    libmosquitto1 libmosquitto-dev mosquitto memcached && apt-get clean

RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && \
    php composer-setup.php && php -r "unlink('composer-setup.php');" && \
    php composer.phar global require composer/composer && rm composer.phar
RUN ln -s  /root/.composer/vendor/bin/composer /usr/bin/composer

RUN composer global require "hirak/prestissimo" "^0.3.7" && \
    composer global require "phpstan/phpstan" "^0.11" && ln -s  /root/.composer/vendor/bin/phpstan /usr/bin/phpstan && \
    composer global require "squizlabs/php_codesniffer" "*" && ln -s  /root/.composer/vendor/bin/phpcs /usr/bin/phpcs

RUN pecl install Mosquitto-alpha && echo "extension=mosquitto.so" >> /etc/php/7.3/cli/php.ini

# I use phpdb for code coverate  RUN pecl install xdebug && rm -rf /tmp/pear && echo "extension=xdebug.so" >> /etc/php/7.3/cli/php.ini

#docker build --pull . -t registry.gitlab.com/notdefine/heating-control:ci
#docker push registry.gitlab.com/notdefine/heating-control:ci
